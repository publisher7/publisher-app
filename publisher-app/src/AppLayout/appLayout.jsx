import React from "react";
import MyAppShell from "../views/Header/AppShell";
const Layout = ({ children }) => {
  return (
    <>
      <MyAppShell />
      {children}
      <div>Footer</div>
    </>
  );
};

export default Layout;
