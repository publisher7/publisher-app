import { Navigate, useLocation } from 'react-router-dom';
import { useLocalStorage } from '@mantine/hooks';
import Loader from '../components/SharedComponents/Loader/Loader';

export default function Authenticated({ children, loading }) {
  const location = useLocation();
  const [isLogged] = useLocalStorage({ key: 'isLogged' });

  if (loading) {
    <Loader />;
  }

  if (!isLogged) {
    return <Navigate to="/" state={{ from: location }} />;
  }

  return children;
}
