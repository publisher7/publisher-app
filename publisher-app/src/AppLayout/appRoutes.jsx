import React from 'react';
import { Route, Routes } from 'react-router';
import CreateNewTeam from '../components/CreateNewTeam/CreateNewTeam';
import UpdateUserProfile from '../components/UpdateUserProfile/UpdateUserProfile';
import MyProfile from '../components/Profile/MyProfile';
import Home from '../views/Home';
import NotFound from '../views/NotFound';
import TeamMembers from '../components/TeamMembers/TeamMembers';
import TeamBooks from '../components/TeamBooks/TeamBooks';
import UserProfile from '../components/Profile/UserProfile';
import { Resume } from '../components/AddResume/CreateResume/CreateResumeView';
import SingleItemView from '../components/SingleItemView/SingleItemView';
import UploadFileDropZone from '../components/AddResume/UploadFileDropZone';
import Notifications from '../components/Notifications/Notifications';
import AuthorBooks from '../components/AuthorBook/AuthorBooks';
import MyItems from '../components/MyItems/MyItems';
import SingleBook from '../components/AuthorBook/SingleBook';
import Authenticated from './Authenticated';
import UserReviews from '../components/UserReviews/UserReviews';

export default function appRoutes({ loading }) {
  const authenticatedRoutes = [
    {
      path: 'create-team',
      component: <CreateNewTeam />,
    },
    {
      path: 'form',
      component: <UploadFileDropZone />,
    },
    {
      path: 'create',
      component: <Resume />,
    },
    {
      path: 'books/:bookId',
      component: <SingleItemView />,
    },
    {
      path: 'author-books/:authorName',
      component: <AuthorBooks />,
    },
    {
      path: 'author-books/:authorName/:bookId',
      component: <SingleBook />,
    },
    {
      path: 'notifications',
      component: <Notifications />,
    },
    {
      path: 'edit-profile',
      component: <UpdateUserProfile />,
    },
    {
      path: 'my-profile',
      component: <MyProfile />,
    },
    {
      path: 'my-teams/members/:teamName',
      component: <TeamMembers />,
    },
    {
      path: 'my-teams/books/:teamName',
      component: <TeamBooks />,
    },
    {
      path: 'profile/:username',
      component: <UserProfile />,
    },
    {
      path: 'my-reviews',
      component: <UserReviews />,
    },
  ];

  return (
    <Routes>
      <Route index element={<Home />} />
      <Route path={'*'} element={<NotFound />} />
      <Route path={'card'} element={<MyItems />} />
      <Route path={'error'} element={<NotFound />} />
      {authenticatedRoutes.map(({ path, component }) => (
        <Route
          key={path}
          path={path}
          element={<Authenticated loading={loading}>{component}</Authenticated>}
        />
      ))}
    </Routes>
  );
}
