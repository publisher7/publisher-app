import { ColorSchemeProvider, MantineProvider } from '@mantine/core';
import { NotificationsProvider } from '@mantine/notifications';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useState, useEffect } from 'react';
import { auth } from './config/firebase-config';
import { BrowserRouter } from 'react-router-dom';
import AppRoutes from './AppLayout/appRoutes';
import MyAppShell from './views/Header/AppShell';
import { getUserData } from './services/user.services';
import AppContext from './providers/AppContext';

function App() {
  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  });
  let [user, loading, error] = useAuthState(auth);

  if (error) console.log(error);

  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
      .then((snapshot) => {
        if (!snapshot.exists()) {
          throw new Error('Something went wrong!');
        }

        setAppState({
          user,
          userData: snapshot.val()[Object.keys(snapshot.val())[0]],
        });
      })
      .catch((e) => alert(e.message));
  }, [user]);

  const [colorScheme, setColorScheme] = useState('light');
  const toggleColorScheme = (value) =>
    setColorScheme(value || (colorScheme === 'dark' ? 'light' : 'dark'));

  return (
    <BrowserRouter>
      <AppContext.Provider value={{ ...appState, setContext: setAppState }}>
        <ColorSchemeProvider colorScheme={colorScheme} toggleColorScheme={toggleColorScheme}>
          <MantineProvider
            theme={{
              colorScheme,
              breakpoints: {
                sm: 800,
                lg: 1275,
              },
            }}
            withGlobalStyles
            withNormalizeCSS
          >
            <NotificationsProvider position="top-right" zIndex={2077}>
              <MyAppShell>
                <AppRoutes loading={loading} />
              </MyAppShell>
            </NotificationsProvider>
          </MantineProvider>
        </ColorSchemeProvider>
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;
