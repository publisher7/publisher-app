import React, { useRef } from 'react';
import {
  Paper,
  Text,
  TextInput,
  Textarea,
  Group,
  SimpleGrid,
  createStyles,
  useMantineTheme,
} from '@mantine/core';

import { ContactIconsList } from './ContactIcons';
import emailjs from '@emailjs/browser';
import { ButtonPrimary } from '../components/SharedComponents/Buttons';

const useStyles = createStyles((theme) => {
  const BREAKPOINT = theme.fn.smallerThan('sm');

  return {
    wrapper: {
      display: 'flex',
      backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.white,
      borderRadius: theme.radius.lg,
      padding: 4,
      border: `1px solid ${
        theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[2]
      }`,

      [BREAKPOINT]: {
        flexDirection: 'column',
      },
    },

    form: {
      boxSizing: 'border-box',
      flex: 1,
      padding: theme.spacing.xl,
      paddingLeft: theme.spacing.xl * 2,
      borderLeft: 0,

      [BREAKPOINT]: {
        padding: theme.spacing.md,
        paddingLeft: theme.spacing.md,
      },
    },

    fields: {
      marginTop: -12,
    },

    fieldInput: {
      flex: 1,

      '& + &': {
        marginLeft: theme.spacing.md,

        [BREAKPOINT]: {
          marginLeft: 0,
          marginTop: theme.spacing.md,
        },
      },
    },

    fieldsGroup: {
      display: 'flex',

      [BREAKPOINT]: {
        flexDirection: 'column',
      },
    },

    contacts: {
      boxSizing: 'border-box',
      position: 'relative',
      borderRadius: theme.radius.lg - 2,
      backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.red[1],
      // backgroundImage: `url(https://raw.githubusercontent.com/mantinedev/ui.mantine.dev/3aed5b9a737a4f865910e5410a655d8efd8b0654/components/GetInTouch/bg.svg)`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      border: '1px solid transparent',
      padding: theme.spacing.xl,
      flex: '0 0 280px',

      [BREAKPOINT]: {
        marginBottom: theme.spacing.sm,
        paddingLeft: theme.spacing.md,
      },
    },

    title: {
      marginBottom: theme.spacing.xl * 1.5,
      fontFamily: `Greycliff CF, ${theme.fontFamily}`,

      [BREAKPOINT]: {
        marginBottom: theme.spacing.xl,
      },
    },

    control: {
      [BREAKPOINT]: {
        flex: 1,
      },
    },
  };
});

export function ContactUs() {
  const theme = useMantineTheme();

  const { classes } = useStyles();
  const form = useRef();
  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm('service_0tb79s9', 'template_22wmzoz', form.current, 'hmfy2uCWR1KpYmAov').then(
      (result) => {
        console.log(result.text);
      },
      (error) => {
        console.log(error.text);
      },
    );
    e.target.reset();
  };

  return (
    <Paper shadow="md" radius="lg">
      <div className={classes.wrapper}>
        <div className={classes.contacts}>
          <Text
            size="lg"
            weight={700}
            className={classes.title}
            style={{
              color: theme.colors.gray[7],
            }}
          >
            Contact information
          </Text>

          <ContactIconsList variant="white" />
        </div>

        <form ref={form} className={classes.form} onSubmit={sendEmail}>
          <Text size="lg" weight={700} className={classes.title}>
            Get in touch
          </Text>

          <div className={classes.fields}>
            <SimpleGrid cols={2} breakpoints={[{ maxWidth: 'sm', cols: 1 }]}>
              <TextInput label="Your name" placeholder="Your name" type="name" name="name" />
              <TextInput
                label="Your email"
                placeholder="hello@mantine.dev"
                required
                type="email"
                name="email"
              />
            </SimpleGrid>

            <TextInput mt="md" label="Subject" placeholder="Subject" required type="subject" />

            <Textarea
              mt="md"
              label="Your message"
              placeholder="Please include all relevant information"
              minRows={3}
              itemType="message"
              name="message"
            />

            <Group position="right" mt="md">
              <ButtonPrimary type="submit" className={classes.control} text={'Send message'} />
            </Group>
          </div>
        </form>
      </div>
    </Paper>
  );
}
