import { useState } from 'react';
import { createStyles, Container, Title, Text, Button } from '@mantine/core';
import Creators from '../components/Creators/Creators';
import { OurTeams } from './OurTeams';
import { ContactUs } from './ContactUs';
import logo2 from './bookFinal2.png';

import SignIn from '../components/SignIn/SignIn';
import { useLocalStorage } from '@mantine/hooks';

export default function Home() {
  const [isLogged, setIsLogged] = useLocalStorage({ key: 'isLogged', defaultValue: false });
  const [openSignIn, setOpenSignIn] = useState(false);

  const { classes } = useStyles();

  return (
    <>
      <div className={classes.root}>
        <Container size="lg">
          <div className={classes.inner}>
            <div className={classes.content}>
              <Title className={classes.title}>
                Spend time{' '}
                <Text
                  component="span"
                  inherit
                  variant="gradient"
                  gradient={{ from: '#ed6ea0', to: '#ec8c69', deg: 35 }}
                >
                  DOING
                </Text>{' '}
                what You{' '}
                <Text
                  component="span"
                  inherit
                  variant="gradient"
                  gradient={{ from: '#ed6ea0', to: '#ec8c69', deg: 35 }}
                >
                  LOVE
                </Text>
                {', '}
                We'll do{' '}
                <Text
                  component="span"
                  inherit
                  variant="gradient"
                  gradient={{ from: '#ed6ea0', to: '#ec8c69', deg: 35 }}
                >
                  All the Rest
                </Text>{' '}
              </Title>
              {/* <Image style={{ color: 'rgb(70 0 100)' }} src={logo} radius="md" width={'150px'} height={40} mt={-10}>
                The Publisher
              </Image> */}
              <Text className={classes.description} mt={30}>
                The Publisher is now Digital for you. Bringing our colleagues expertise, we could
                ensure that each book is unique and professional.
                <br /> Our team of review, editorial, design and marketing experts can turn any idea
                into a beautiful, finished book that stands out from the crowd. <br /> All this is
                possible without any personal face-to-face meeting <br /> You are just one click
                away from publishing your dream book!
              </Text>
              {!isLogged && (
                <Button
                  onClick={() => setOpenSignIn(true)}
                  variant="gradient"
                  gradient={{ from: '#ed6ea0', to: '#ec8c69', deg: 35 }}
                  size="xl"
                  className={classes.control}
                  mt={40}
                >
                  Register / Sign In
                </Button>
              )}
            </div>
          </div>
        </Container>
      </div>
      <OurTeams />
      <Container size="lg" className={classes.titleHead}>
        <Title align="center" order={1}>
          Our Support Team
        </Title>
      </Container>
      <br />
      <ContactUs />
      <br />
      <Container size="lg" className={classes.titleHead}>
        <Title align="center" order={1}>
          The Publisher Creators
        </Title>
      </Container>
      <Creators />
      <SignIn openSignIn={openSignIn} setOpenSignIn={setOpenSignIn} setIsLogged={setIsLogged} />
    </>
  );
}

const useStyles = createStyles((theme) => ({
  root: {
    backgroundColor: '#fff4f4',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundImage: `linear-gradient(250deg, rgba(130, 201, 30, 0) 0%, #062343 70%), url(${logo2})`,
    paddingTop: theme.spacing.xl * 3,
  },

  inner: {
    display: 'flex',
    justifyContent: 'space-between',

    [theme.fn.smallerThan('md')]: {
      flexDirection: 'column',
    },
  },

  image: {
    [theme.fn.smallerThan('md')]: {
      display: 'none',
    },
  },

  content: {
    paddingTop: theme.spacing.xl * 2,
    paddingBottom: theme.spacing.xl * 2,
    marginRight: theme.spacing.xl * 3,

    [theme.fn.smallerThan('md')]: {
      marginRight: 0,
    },
  },

  title: {
    color: theme.colors.gray[4],
    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
    fontWeight: 700,
    lineHeight: 1.05,
    maxWidth: 500,
    fontSize: 48,
    letterSpacing: 2,

    [theme.fn.smallerThan('md')]: {
      maxWidth: '100%',
      fontSize: 34,
      lineHeight: 1.15,
    },
  },

  description: {
    color: theme.white,
    opacity: 0.75,
    maxWidth: 500,

    [theme.fn.smallerThan('md')]: {
      maxWidth: '100%',
    },
  },
  titleHead: {
    color: theme.colors.gray[7],
    paddingTop: '10px',
    marginBottom: '20px',
    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
    fontWeight: 700,
    lineHeight: 1.05,
    maxWidth: 800,
    fontSize: 35,

    [theme.fn.smallerThan('md')]: {
      maxWidth: '100%',
      fontSize: 34,
      lineHeight: 1.15,
    },
  },
  control: {
    paddingLeft: 50,
    paddingRight: 50,
    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
    fontSize: 22,

    [theme.fn.smallerThan('md')]: {
      width: '100%',
    },
  },
}));
