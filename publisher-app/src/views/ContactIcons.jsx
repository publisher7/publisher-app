import React from 'react';
import { createStyles, ThemeIcon, Text, Group, SimpleGrid, Box, useMantineTheme } from '@mantine/core';
import { Sun, Phone, MapPin, At } from 'tabler-icons-react';

const useStyles = createStyles((theme, { variant }) => ({
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    color: theme.white,
  },

  icon: {
    marginRight: theme.spacing.md,
    backgroundImage:
      variant === 'gradient'
        ? `linear-gradient(135deg, ${theme.colors[theme.primaryColor][1]} 0%, ${
            theme.colors[theme.primaryColor][6]
          } 100%)`
        : `linear-gradient(135deg, ${theme.colors[theme.primaryColor][1]} 0%, ${
            theme.colors[theme.primaryColor][3]
          } 100%)`,
    backgroundColor: 'transparent',
  },

  title: {
    color: variant === 'gradient' ? theme.colors[theme.primaryColor][1] : theme.colors.gray[6],
  },

  description: {
    color: variant === 'gradient' ? theme.white : theme.colors.gray[7],
  },
}));

function ContactIcon({ icon: Icon, title, description, variant = 'gradient', className, ...others }) {
  const { classes, cx } = useStyles({ variant });
  const theme = useMantineTheme();

  return (
    <div className={cx(classes.wrapper, className)} {...others}>
      {variant === 'gradient' ? (
        <ThemeIcon size={40} radius="md" className={classes.icon}>
          <Icon size={24} />
        </ThemeIcon>
      ) : (
        <Box mr="md">
          <Icon
            size={24}
            style={{
              color: variant === 'gradient' ? theme.colors[theme.primaryColor][3] : theme.colors.gray[7],
            }}
          />
        </Box>
      )}

      <div>
        <Text
          size="xs"
          className={classes.title}
          style={{
            color: variant === 'gradient' ? theme.colors[theme.primaryColor][3] : theme.colors.gray[7],
          }}
        >
          {title}
        </Text>
        <Text className={classes.description}>{description}</Text>
      </div>
    </div>
  );
}

const MOCKDATA = [
  { title: 'Email', description: 'thepublisherstudio@gmail.com', icon: At },
  { title: 'Phone', description: '+359 88 88 888 88', icon: Phone },
  { title: 'Address', description: 'Everywhere', icon: MapPin },
  { title: 'Working hours', description: '8 a.m. – 11 p.m.', icon: Sun },
];

export function ContactIconsList({ data = MOCKDATA, variant }) {
  const items = data.map((item, index) => <ContactIcon key={index} variant={variant} {...item} />);
  return <Group direction="column">{items}</Group>;
}

export function ContactIcons() {
  return (
    <SimpleGrid cols={2} breakpoints={[{ maxWidth: 755, cols: 1 }]}>
      <Box
        sx={(theme) => ({
          padding: theme.spacing.xl,
          borderRadius: theme.radius.md,
          backgroundColor: theme.white,
        })}
      >
        <ContactIconsList />
      </Box>

      <Box
        sx={(theme) => ({
          padding: theme.spacing.xl,
          borderRadius: theme.radius.md,
          backgroundImage: `linear-gradient(135deg, ${theme.colors[theme.primaryColor][6]} 0%, ${
            theme.colors[theme.primaryColor][4]
          } 100%)`,
        })}
      >
        <ContactIconsList variant="gradient" />
      </Box>
    </SimpleGrid>
  );
}
