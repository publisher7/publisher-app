import React from 'react';
import { createStyles, Text, SimpleGrid, Container, Title, Group } from '@mantine/core';
import {
  Certificate,
  Coin,
  Book,
  ReportMoney,
  Printer,
  SortAscendingLetters,
} from 'tabler-icons-react';
import { teamNames } from '../common/helpers';

const useStyles = createStyles((theme) => ({
  root: {
    width: '100%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: theme.spacing.xl,
  },
  feature: {
    position: 'relative',
    paddingTop: theme.spacing.xl,
  },

  overlay: {
    position: 'absolute',
    height: 100,
    width: 160,
    top: 0,
    left: 0,
    backgroundColor:
      theme.colorScheme === 'dark'
        ? theme.fn.rgba(theme.colors[theme.primaryColor][7], 0.2)
        : theme.colors.pink[0],
    zIndex: 1,
  },

  content: {
    position: 'relative',
    zIndex: 2,
  },

  icon: {
    color: theme.colors[theme.primaryColor][theme.colorScheme === 'dark' ? 4 : 6],
  },
  titleHead: {
    color: theme.colors.gray[7],
    paddingTop: '10px',
    marginBottom: '20px',
    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
    fontWeight: 700,
    lineHeight: 1.05,
    maxWidth: 800,
    fontSize: 35,

    [theme.fn.smallerThan('md')]: {
      maxWidth: '100%',
      fontSize: 34,
      lineHeight: 1.15,
    },
  },
  title: {
    color: theme.colorScheme === 'dark' ? theme.white : theme.black,
    fontWeight: 500,
  },
}));

function Feature({ icon: Icon, title, description, className, ...others }) {
  const { classes, cx } = useStyles();

  return (
    <div className={cx(classes.feature, className)} {...others}>
      <div className={classes.overlay} />

      <div className={classes.content}>
        <Icon size={38} className={classes.icon} />
        <Text weight={700} size="lg" mb="xs" mt={5} className={classes.title}>
          {title}
        </Text>
        <Text color="dimmed" size="sm">
          {description}
        </Text>
      </div>
    </div>
  );
}

const mockdata = [
  {
    icon: Book,
    title: teamNames[0],
    description:
      'Here we will read careful your book and will review if we are the right publisher for your book',
  },
  {
    icon: Certificate,
    title: teamNames[1],
    description: 'Here we will give you the best deal for publishing your book',
  },
  {
    icon: SortAscendingLetters,
    title: teamNames[2],
    description:
      'Here we will carefully transform your script into a book format and will revise all typos.',
  },
  {
    icon: Printer,
    title: teamNames[3],
    description: 'Here we will made the perfect cover for your book.',
  },
  {
    icon: ReportMoney,
    title: teamNames[4],
    description: 'Here we will make your book recognizable and that it will stand out.',
  },
  {
    icon: Coin,
    title: teamNames[5] || 'Sales',
    description: 'Your track of the sales.',
  },
];

export function OurTeams() {
  const { classes } = useStyles();

  const items = mockdata.map((item) => <Feature {...item} key={item.title} />);

  return (
    <Container size="lg" className={classes.root} width={'100%'}>
      <Group position="center" align="center">
        <Title order={5} className={classes.titleHead}>
          Our Teams that will make your book stand out!
          <SimpleGrid cols={3} breakpoints={[{ maxWidth: 'sm', cols: 1 }]} spacing={50} mt={30}>
            {items}
          </SimpleGrid>
        </Title>
      </Group>
    </Container>
  );
}
