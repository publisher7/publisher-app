import { AppShell } from '@mantine/core';
import React, { useState } from 'react';
import AppFooter from '../../components/Footer/AppFooter';
import AppNavbar from '../../components/NavBar/AppNavbar';
import AppSideBar from '../../components/SideBar/AppSideBar';

export default function MyAppShell({ children }) {
  const [openedResponsiveSideBar, setOpenedResponsiveSideBar] = useState(false);

  return (
    <AppShell
      padding="md"
      navbarOffsetBreakpoint="sm"
      asideOffsetBreakpoint="sm"
      fixed
      navbar={
        <AppSideBar
          hidden={!openedResponsiveSideBar}
          hiddenBreakpoint="sm"
          showHideSidebar={setOpenedResponsiveSideBar}
        />
      }
      header={<AppNavbar opened={openedResponsiveSideBar} setOpened={setOpenedResponsiveSideBar} />}
      footer={<AppFooter />}
      styles={(theme) => ({
        main: {
          backgroundColor:
            theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
          paddingLeft: '20px))',
        },
      })}
    >
      {children}
    </AppShell>
  );
}
