import { useNavigate } from 'react-router';
import './NotFound.css';

export default function NotFound() {
  const navigate = useNavigate();

  return (
    <div className="error-page">
      <div className="error-page__container">
        <h2>Publisher</h2>
        <div className="error-number__container">
          <div className="dash-container">
            <div className="dash"></div>
          </div>
          <div className="number">404</div>
          <div className="dash-container">
            <div className="dash"></div>
          </div>
        </div>
        <p className="not-found">uh-oh! Page not found</p>

        <div className="quote">
          <p>"Not all those who wander are lost."</p>
          <p> - J.R.R. Tolkien, The Fellowship of the Ring</p>
        </div>
        <button className="button" type="button" onClick={() => navigate('/')}>
          Go back to the beginning
        </button>
        <img
          className="image-book"
          src="https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/projectImages%2Ferror-page.png?alt=media&token=6acaa02d-e656-44e7-a3ae-ebf07eba5c8e"
          alt=""
        />
      </div>
    </div>
  );
}
