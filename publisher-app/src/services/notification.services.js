import {get, ref, push, update, onValue} from 'firebase/database';
import {db} from '../config/firebase-config';

export const getLiveNotifications = (username, listen) => {
	return onValue(ref(db, `users/${username}/notifications`), listen);
};

export const sendNotification = (usersNames, message) => {
	// console.log(message.toString());
	return push(ref(db, 'notifications'), {
		message,
		createdOn: Date.now(),
	})
		.then((result) => {
			const updateDB = usersNames.reduce((acc, username) => {
				acc[`users/${username}/notifications/${result.key}`] = 'unread';
				return acc;
			}, {});

			return update(ref(db), updateDB);
		})
		.catch((err) => console.log(err));
};

export const getUserNotifications = (snapshot) => {
	const notificationsIds = snapshot.val();

	return Object.keys(notificationsIds).map((id) => {
		return get(ref(db, `notifications/${id}`))
			.then((result) => ({...result.val(), id}))
			.catch((err) => console.log(err));
	});
};

export const deleteUserNotification = (username, id) => {
	return update(ref(db), {
		[`users/${username}/notifications/${id}`]: null,
	}).catch((err) => console.log(err));
};

export const deleteAllUserNotifications = (username) => {
	return update(ref(db), {
		[`users/${username}/notifications/`]: null,
	}).catch((err) => console.log(err));
};

export const getNotificationsCount = (username) => {
	return get(ref(db, `users/${username}/notifications`))
		.then((result) => Object.values(result.val()).filter((notif) => notif.includes('un')).length)
		.catch((err) => console.log(err));
};

export const setNotificationsAsRead = (username) => {
	return get(ref(db, `users/${username}/notifications`))
		.then((result) => {
			if (!result.val()) return;

			return Object.keys(result.val()).map((id) => {
				return update(ref(db), {
					[`users/${username}/notifications/${id}`]: 'read',
				});
			});
		})
		.catch((err) => console.log(err));
};
