import {get, ref, update, onValue, push, runTransaction} from 'firebase/database';
import {teamNames} from '../common/helpers';
import {db} from '../config/firebase-config';

export const addBook = (file, username) => {
	const {title, genre, files, resume} = file;

	return push(
		ref(db, 'books'),
		{
			title,
			teams: {
				[`${teamNames[0]}`]: false,
				[`${teamNames[1]}`]: false,
				[`${teamNames[2]}`]: false,
				[`${teamNames[3]}`]: false,
				[`${teamNames[4]}`]: false,
			},
			author: username,
			createdOn: Date.now(),
			genre,
			files,
			resume,
			currentTeam: 1,
			reviewedStatus: 'pending',
			reviews: {
				firstReviewer: {
					status: '',
					name: '',
				},
				secondReviewer: {
					status: '',
					name: '',
				},
			},
		},
		ref(db, 'books/reviews'),
		{
			status: 'pending',
			firstReviewer: null,
			secondReviewer: null,
		},
	).then((result) => {
		return getBookById(result.key).then(() => {
			const resultId = result.key;

			return update(ref(db), {
				[`/teams/${teamNames[0]}/books/${resultId}`]: resultId,
				[`users/${username}/authorBooks/${resultId}`]: true,
				[`books/${resultId}/id`]: resultId,
				[`books/${resultId}/reviewedStatus`]: 'pending',
			});
		});
	});
};

export const getBookById = (id) => {
	return get(ref(db, `books/${id}`)).then((result) => {
		if (!result.exists()) {
			throw new Error(`Comment with id ${id} does not exist!`);
		}
		const book = result.val();
		return book;
	});
};

export const getLiveBooks = (listen) => {
	return onValue(ref(db, 'books'), listen);
};
export const getLiveSingleBook = (bookId, listen) => {
	return onValue(ref(db, `books/${bookId}`), listen);
};
export const getLiveTeamBooks = (teamName, listen) => {
	return onValue(ref(db, `teams/${teamName}/books`), listen);
};

export const getLiveAuthorBooks = (username, listen) => {
	return onValue(ref(db, `users/${username}/authorBooks`), listen);
};
export const getLiveTeamMembers = (teamName, listen) => {
	return onValue(ref(db, `teams/${teamName}/members`), listen);
};

export const getAllBooks = () => {
	return get(ref(db, 'books')).then((snapshot) => {
		if (!snapshot.exists()) {
			return new Error('does not exit');
		}
		return fromBooksDocument(snapshot);
	});
};

export const updateStatus = async (
	bookId,
	firstReviewerStatus = '',
	firstReviewerName = '',
	secondReviewerStatus = '',
	secondReviewerName = '',
) => {
	// const post = await getPostById(id);
	return update(ref(db), {
		[`/books/${bookId}/reviews/firstReviewer/status`]: firstReviewerStatus,
		[`/books/${bookId}/reviews/firstReviewer/name`]: firstReviewerName,
		[`/books/${bookId}/reviews/secondReviewer/status`]: secondReviewerStatus,
		[`/books/${bookId}/reviews/secondReviewer/name`]: secondReviewerName,
	});
};

export const updateFirstReviewerName = async (bookId, value) => {
	return update(ref(db), {
		[`/books/${bookId}/reviews/firstReviewer/name`]: value,
	});
};
export const updateSecondReviewerName = async (bookId, value) => {
	return update(ref(db), {
		[`/books/${bookId}/reviews/secondReviewer/name`]: value,
	});
};

export const updateFirstReviewerStatus = async (bookId, value) => {
	return update(ref(db), {
		[`/books/${bookId}/reviews/firstReviewer/status`]: value,
	});
};
export const updateSecondReviewerStatus = async (bookId, value) => {
	return update(ref(db), {
		[`/books/${bookId}/reviews/secondReviewer/status`]: value,
	});
};
export const updateTeam2Status = async (bookId) => {
	return update(ref(db), {
		[`/books/${bookId}/team2/status`]: 'first',
	});
};

export const updateBookToAnotherTeam = (bookId, teamName, name = '', currentTeam) => {
	return update(ref(db), {
		[`/books/${bookId}/reviews/firstReviewer/status`]: '',
		[`/books/${bookId}/reviews/firstReviewer/name`]: '',
		[`/books/${bookId}/reviews/secondReviewer/name`]: '',
		[`/books/${bookId}/reviews/secondReviewer/status`]: '',
		[`/books/${bookId}/reviewedStatus`]: 'pending',
		[`/teams/${teamName}/books/${bookId}`]: bookId,
		[`/books/${bookId}/teams/${currentTeam}`]: true,
	}).then(() =>
		runTransaction(ref(db, `/books/${bookId}`), (book) => {
			book.currentTeam++;
			return book;
		}),
	);
};
export const addNewFile = async (bookId, file) => {
	const files = await get(ref(db, `books/${bookId}/files`)).then((result) => {
		return result.val();
	});
	if (files) {
		return update(ref(db), {
			[`/books/${bookId}/files`]: [...file, ...files],
		});
	}
	return update(ref(db), {
		[`/books/${bookId}/files`]: [...file],
	});
};
export const updateSecondTeamBookStatus = async (bookId, value, bookStatus = 'pending') => {
	return update(ref(db), {
		[`/books/${bookId}/team2/status`]: value,
		[`/books/${bookId}/reviewedStatus/`]: bookStatus,
	});
};
export const addAuthorsContractFile = (bookId, files) => {
	return update(ref(db), {
		[`/books/${bookId}/contract/files`]: files,
	});
};
export const updateContractReviewer = (bookId, username) => {
	return update(ref(db), {
		[`/books/${bookId}/contract/reviewer`]: username,
	});
};
export const updateThirdTeamReviewer = (bookId, username) => {
	return update(ref(db), {
		[`/books/${bookId}/team3/reviewer`]: username,
	});
};
export const updateFourthTeamReviewer = (bookId, username) => {
	return update(ref(db), {
		[`/books/${bookId}/team4/reviewer`]: username,
	});
};

export const updateThirdTeamFile = (bookId, files) => {
	return update(ref(db), {
		[`/books/${bookId}/team3/files`]: files,
	});
};
export const updateFourthTeamFile = (bookId, files) => {
	return update(ref(db), {
		[`/books/${bookId}/team4/files`]: files,
	});
};
export const updateBookDesign = (bookId) => {
	return update(ref(db), {
		[`/books/${bookId}/team4/design`]: true,
	});
};
export const updateFourthTeamFileImage = (bookId, files) => {
	return update(ref(db), {
		[`/books/${bookId}/team4/image`]: files,
	});
};
export const updateThirdTeamBookStatus = async (bookId, bookStatus = 'pending', teamName) => {
	console.log(teamName);
	return update(ref(db), {
		[`/books/${bookId}/reviewedStatus/`]: bookStatus,
		[`/books/${bookId}/teams/${teamNames[teamName - 1]}`]: true,
	});
};
export const updateFourthTeamBookStatus = async (bookId, bookStatus = 'pending', teamName) => {
	return update(ref(db), {
		[`/books/${bookId}/reviewedStatus/`]: bookStatus,
		[`/books/${bookId}/teams/${teamNames[teamName - 1]}`]: true,
	});
};
export const updateFinalStatus = async (bookId) => {
	return update(ref(db), {
		[`/books/${bookId}/finalStatus/`]: 'approved',
	});
};

export const addNewContractFile = async (bookId, file) => {
	const files = await get(ref(db, `/books/${bookId}/contract/files`)).then((result) => {
		return result.val();
	});
	if (files) {
		return update(ref(db), {
			[`/books/${bookId}/contract/files`]: [...files, ...file],
		});
	}
	return update(ref(db), {
		[`/books/${bookId}/contract/files`]: [...file],
	});
};
export const fromBooksDocument = (snapshot) => {
	const booksDocument = snapshot.val();
	if (booksDocument) {
		return Object.keys(booksDocument).map((key) => {
			const book = booksDocument[key];
			const created = Date.now();
			const createdDate = new Date(created);
			return {
				...book,
				id: key,
				createdOn: createdDate,
			};
		});
	}
};

export const getTeamMembersByTeamName = (teamName) => {
	return get(ref(db, `teams/${teamName}/members`));
};
export const updateCurrentTeamStatus = async (bookId) => {
	const snapshot = await get(ref(db, `books/${bookId}/currentTeam/`));
	let result = snapshot.val();
	return update(ref(db), {
		[`books/${bookId}/currentTeam/`]: ++result,
	});
};
export const deleteBook = (bookId, teamName) => {
	return update(ref(db), {
		[`books/${bookId}`]: null,
		[`/teams/${teamName}/books/${bookId}`]: null,
	});
};
export const updateBookStatus = (bookId, status) => {
	return update(ref(db), {
		[`books/${bookId}/reviewedStatus`]: status,
	});
};

export const updateTeamBook = (bookId, teamName) => {
	return update(ref(db), {
		[`/teams/${teamName}/books/${bookId}`]: bookId,
	});
};
