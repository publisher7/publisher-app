import {
	get,
	set,
	ref,
	query,
	equalTo,
	orderByChild,
	update,
	onDisconnect,
	onValue,
	serverTimestamp,
	runTransaction,
} from 'firebase/database';
import {db, auth} from '../config/firebase-config';

/**
 * Takes a snapshot and returns an array of objects with users data.
 * @param {Snapshot} snapshot - A firebase snapshot
 * @returns {Array.<{id: string,username: string, uid:string,isLogged: boolean email: string, lastOnline: number, firstName: string, lastName: string, phoneNumber: string, role: string,createdOn: number, teams?: {}, notifications?: {}>}}
 */
export const fromUsersDocument = (snapshot) => {
	const usersDocument = snapshot.val();

	return Object.keys(usersDocument).map((key) => {
		const user = usersDocument[key];

		return {
			...user,
			id: key,
		};
	});
};

/**
 * @param {string} username - The username.
 * @returns {Promise<DataSnapshot>} A promise that returns snapshot if resolved.
 */
export const getUserByUsername = (username) => {
	return get(ref(db, `users/${username}`));
};

/**
 * @param {string} uid - The user uid.
 * @returns {Promise<DataSnapshot>} A promise that returns snapshot if resolved.
 */
export const getUserData = (uid) => {
	return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

/**
 * Accepts a listener and returns a onValue function from db.
 * @see https://firebase.google.com/docs/database/web/read-and-write
 * @param {<callback>} listen
 * @returns {Function} onValue
 */
export const getLiveUsers = (listen) => {
	return onValue(ref(db, 'users'), listen);
};

/**
 * Adds addition info for the user to the DB when signing up.
 * @param {string} username - User username
 * @param {string} uid - User uid
 * @param {string} email - User email
 * @param {string} firstName - User first name
 * @param {string} lastName - User last name
 * @param {string} phoneNumber - User phone number
 * @returns {Promise<void>} - Resolves when write to server is complete.
 */
export const addAdditionalUserInfo = (
	username,
	uid,
	email,
	firstName,
	lastName,
	phoneNumber,
	code,
) => {
	let role;
	if (code === 'PubEmp') role = 'employee';
	if (code === 'PubAdm') role = 'admin';
	if (code === '') role = 'author';

	return set(ref(db, `users/${username}`), {
		username,
		uid,
		email,
		firstName,
		lastName,
		phoneNumber,
		role: role,
		isLogged: true,
		lastOnline: Date.now(),
		createdOn: Date.now(),
		currentlyReviewing: 0,
		totalReviewed: 0,
		avatarUrl:
			'https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/projectImages%2Fdefault-profile-avatar.png?alt=media&token=f6771948-19fb-4012-826d-5a4867f4f0b2',
	});
};

/**
 * Gets a snapshot of all users in the DB.
 @returns {Promise<DataSnapshot>} A promise that returns a snapshot if resolved.
 */
export const getAllUsers = () => {
	return get(ref(db, 'users/'));
};

/**
 * Updates the user email in DB.
 * @param {string} username - User username.
 * @param {string} email - User new email.
 * @returns {Promise.<void>} - Resolves when update on server is complete.
 */
export const updateDBUserEmail = (username, email) => {
	return update(ref(db), {
		[`users/${username}/email`]: email,
	});
};

/**
 * Updates the user profile picture.
 * @param {string} username - User username.
 * @param {string} url - The url of the in firebase storage.
 * @returns {Promise.<void>} - Resolves when update on server is complete.
 */
export const updateUserProfilePicture = (username, url) => {
	return update(ref(db), {
		[`users/${username}/avatarUrl`]: url,
	});
};

/**
 * Resets the user picture to the default Avatar.
 * @param {string} username
 * @returns {Promise.<void>} - Resolves when update on server is complete.
 */
export const reverseDefaultAvatar = (username) => {
	const url =
		'https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/projectImages%2Fdefault-profile-avatar.png?alt=media&token=f6771948-19fb-4012-826d-5a4867f4f0b2';
	return updateUserProfilePicture(username, url);
};

/**
 * Changes the logged user status in DB.
 * @param {string} username
 * @param {boolean=} boolean If false - sets the users to offline and updates the lastOnline prop.
 * @returns {Promise.<void>} - Resolves when update on server is complete.
 */
export const changeLoggedStatus = (username, boolean = true) => {
	if (!boolean) {
		update(ref(db), {
			[`users/${username}/lastOnline`]: serverTimestamp(),
		}).catch((err) => console.log(err));
	}
	return update(ref(db), {
		[`users/${username}/isLogged`]: boolean,
	}).catch((err) => console.log(err));
};

export const getLiveUserTeams = (username, listen) => {
	return onValue(ref(db, `users/${username}/teams`), listen);
};

export const addToUserReviewing = (username) => {
	runTransaction(ref(db, `/users/${username}/currentlyReviewing`), (count) => {
		return ++count;
	});
};

export const removeFromUserReviewing = (usersNames) => {
	console.log(usersNames);

	const promises = usersNames.map((username) =>
		runTransaction(ref(db, `/users/${username}/currentlyReviewing`), (count) => {
			--count;
			return count;
		}),
	);

	Promise.all(promises).catch((err) => console.log(err));
};

export const addToUserTotalReviews = (usersNames) => {
	console.log(usersNames);
	const promises = usersNames.map((username) =>
		runTransaction(ref(db, `/users/${username}/totalReviewed`), (count) => {
			++count;
			return count;
		}),
	);

	Promise.all(promises).catch((err) => console.log(err));
};

const connectedRef = ref(db, '.info/connected');
onValue(connectedRef, async (snap) => {
	let username;
	if (snap.val() === true) {
		try {
			if (auth.currentUser !== null) {
				const snapshot = await getUserData(auth.currentUser.uid);
				username = fromUsersDocument(snapshot)[0].username;
				update(ref(db), {
					[`users/${username}/isLogged`]: true,
				});
			}
		} catch (err) {
			console.log(err);
		}
	}

	if (username) {
		const myRef = ref(db, `users/${username}`);
		onDisconnect(myRef).update({isLogged: false, lastOnline: serverTimestamp()});
	}
});
