import {
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  sendPasswordResetEmail,
  signInWithEmailAndPassword,
  signOut,
  updateEmail,
  updatePassword,
} from 'firebase/auth';
import { auth } from '../config/firebase-config';

export const signup = (email, password) => {
  return createUserWithEmailAndPassword(auth, email, password);
};

export const login = (email, password) => {
  return signInWithEmailAndPassword(auth, email, password);
};

export const logout = () => {
  return signOut(auth);
};

export const updateUserEmail = (user, email) => {
  return updateEmail(user, email);
};

export const updateUserPassword = (user, password) => {
  return updatePassword(user, password);
};

export const resetPassword = (email) => {
  return sendPasswordResetEmail(auth, email);
};
