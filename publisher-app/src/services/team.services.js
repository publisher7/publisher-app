import { get, set, ref, query, equalTo, orderByChild, update, onValue } from 'firebase/database';
import { db } from '../config/firebase-config';
import { sendNotification } from './notification.services';

export const getLiveTeams = (listen) => {
  return onValue(ref(db, 'teams'), listen);
};

export const getLiveSingleTeam = (teamName, listen) => {
  return onValue(ref(db, `teams/${teamName}`), listen);
};

export const getTeamByTeamName = (teamName) => {
  return get(ref(db, `teams/${teamName}`));
};

export const getUserTeamRole = (teamName, username) => {
  return get(ref(db, `teams/${teamName}/members/${username}`));
};

export const createNewTeam = (teamName, ownersUserNames, memberUserNames) => {
  const owners = ownersUserNames.reduce((acc, name) => {
    acc[`${name}`] = 'owner';
    return acc;
  }, {});

  const employees = memberUserNames.reduce((acc, name) => {
    acc[`${name}`] = 'employee';
    return acc;
  }, {});

  return get(ref(db, `teams/${teamName}`))
    .then((snapshot) => {
      if (snapshot.exists()) {
        return 'Error';
      } else {
        return set(ref(db, `teams/${teamName}`), {
          teamName,
          members: {
            ...owners,
            ...employees,
          },
        }).then(() => {
          return Promise.all(
            [...ownersUserNames, ...memberUserNames].map((username) => {
              return update(ref(db), {
                [`users/${username}/teams/${teamName}`]: true,
              });
            }),
          ).then(() => {
            return sendNotification(
              [...ownersUserNames, ...memberUserNames],
              `You have been added to team ${teamName}`,
            );
          });
        });
      }
    })
    .catch((err) => console.log(err));
};

export const addNewMembers = (userNames, teamName) => {
  const updateDB = userNames.reduce((acc, username) => {
    acc[`teams/${teamName}/members/${username}`] = 'employee';
    acc[`users/${username}/teams/${teamName}`] = true;
    return acc;
  }, {});

  return update(ref(db), updateDB).then(() =>
    sendNotification(userNames, `You have been added to team ${teamName}`),
  );
};

export const removeMember = (username, teamName) => {
  update(ref(db), { [`teams/${teamName}/members/${username}`]: null });
  update(ref(db), { [`users/${username}/teams/${teamName}`]: null });
  sendNotification([username], `You have been removed from team ${teamName}`);
};
