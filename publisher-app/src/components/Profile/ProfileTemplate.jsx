import './ProfileTemplate.css';
import { Group, Avatar, Text, Container } from '@mantine/core';
import { Edit } from 'tabler-icons-react';
import { timestampToDate } from '../../common/helpers';
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import AppContext from '../../providers/AppContext';

export default function ProfileTemplate({
  avatarUrl,
  firstName,
  lastName,
  email,
  role,
  username,
  createdOn,
  currentlyReviewing,
  totalReviewed,
}) {
  const { userData } = useContext(AppContext);

  return (
    <Container fluid={true}>
      <Group position="right" mt={userData.username !== username ? 'xl' : null}>
        <Link to="../edit-profile">
          {userData.username === username && (
            <Group>
              <Text size="lg" color="gray">
                Edit
              </Text>
              <Edit className="edit-profile-icon" />
            </Group>
          )}
        </Link>
      </Group>
      <Group position="center">
        <Avatar src={avatarUrl} radius="md" sx={{ width: '120px', height: '120px' }} />
      </Group>
      <Group mt="lg" direction="column" position="center">
        <Text size="xl">{`${firstName} ${lastName}`}</Text>
        <Group position="center">
          <Text size="lg">{`${email}`}</Text>
          <Text size="md">Member since: {`${timestampToDate(createdOn).date}`}</Text>
        </Group>
        {role !== 'author' && (
          <Text size="md">
            Total reviews: {totalReviewed} Currently reviewing: {currentlyReviewing}
          </Text>
        )}
      </Group>
    </Container>
  );
}
