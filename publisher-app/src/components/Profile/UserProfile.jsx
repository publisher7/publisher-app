import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { getUserByUsername } from '../../services/user.services';
import ProfileTemplate from './ProfileTemplate';

export default function UserProfile() {
  const [error, setError] = useState(null);
  const [userData, setUserData] = useState(null);
  const { username } = useParams();

  useEffect(() => {
    getUserByUsername(username).then((snapshot) => {
      if (!snapshot.val()) {
        return setError(true);
      }

      setUserData(() => snapshot.val());
    });
  }, [username]);

  return <>{!error ? <ProfileTemplate {...userData} /> : <div>Oops, something went wrong!</div>}</>;
}
