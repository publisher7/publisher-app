import { useContext } from 'react';
import AppContext from '../../providers/AppContext';
import Loader from '../SharedComponents/Loader/Loader';
import ProfileTemplate from './ProfileTemplate';

export default function MyProfile() {
  const { userData } = useContext(AppContext);
  //TODO: add the teams to which the user belongs
  return <>{userData ? <ProfileTemplate {...userData} /> : <Loader />}</>;
}
