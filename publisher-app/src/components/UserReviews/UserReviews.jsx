import { useContext } from 'react';
import AppContext from '../../providers/AppContext';
import AllItemsView from '../AllItemsView/AllItemsView';
import Loader from '../SharedComponents/Loader/Loader';

export default function UserReviews() {
  const { userData } = useContext(AppContext);

  return <>{userData ? <AllItemsView singleUser={true} userData={userData} /> : <Loader />}</>;
}
