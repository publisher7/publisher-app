import React, { useContext } from 'react';
import AppContext from '../../providers/AppContext';
import AllItemsView from '../AllItemsView/AllItemsView';
import Loader from '../SharedComponents/Loader/Loader';
import TeamBooks from '../TeamBooks/TeamBooks';

export default function MyItems() {
  const { userData } = useContext(AppContext);

  return <>{userData ? <TeamBooks singleUser={true} team={Object.keys(userData.teams)[0]} /> : <Loader />}</>;
}
