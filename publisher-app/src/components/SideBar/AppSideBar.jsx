import React from 'react';
import { Navbar, ScrollArea, useMantineTheme } from '@mantine/core';
import { UserInfoSideBar } from './UserInfoSideBar';
import { useLocalStorage } from '@mantine/hooks';
import { MainLinks } from './SideBarLinks/SideBarLinks';

export default function AppSideBar({ hidden, showHideSidebar }) {
  const [isLogged] = useLocalStorage({ key: 'isLogged' });
  const theme = useMantineTheme();
  return (
    <>
      {isLogged && (
        <Navbar
          p="md"
          hidden={hidden}
          hiddenBreakpoint="sm"
          width={{ sm: 250, lg: 250 }}
          style={{ backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.red[0] }}
        >
          <Navbar.Section grow component={ScrollArea} mt="xs">
            <MainLinks showHideSidebar={showHideSidebar} />
          </Navbar.Section>
          <Navbar.Section>
            <UserInfoSideBar showHideSidebar={showHideSidebar} />
          </Navbar.Section>
        </Navbar>
      )}
    </>
  );
}
