import { useContext, useEffect, useState } from 'react';
import { ThemeIcon, Text, Accordion, Skeleton, Divider } from '@mantine/core';
import AppContext from '../../../providers/AppContext';
import { Link } from 'react-router-dom';
import { getLiveUserTeams } from '../../../services/user.services';
import { getLiveTeams } from '../../../services/team.services';

export default function UserTeamsLink({ icon, color, label, to, showHideSidebar }) {
  const { userData } = useContext(AppContext);
  const [userTeams, setUserTeams] = useState([]);

  useEffect(() => {
    if (!userData) return;

    // Admin
    if (userData.role === 'admin') {
      const unsubscribe = getLiveTeams((snapshot) => {
        if (!snapshot.exists()) return;

        setUserTeams(Object.keys(snapshot.val()).reverse());
      });

      return () => unsubscribe();
    }

    // Employee
    if (userData.role === 'employee') {
      const unsubscribe = getLiveUserTeams(userData?.username, (snapshot) => {
        if (!snapshot.exists()) return;

        setUserTeams(Object.keys(snapshot.val()).reverse());
      });

      return () => unsubscribe();
    }
  }, [userData]);

  return (
    <>
      {userData ? (
        <Accordion iconPosition="right">
          <Accordion.Item
            label={
              <>
                <ThemeIcon color={color} variant="light">
                  {icon}
                </ThemeIcon>
                <Text size="sm" weight={400} className="Accordion-item-heading">
                  {label}
                </Text>
              </>
            }
          >
            {userTeams.length > 0 ? (
              userTeams.map((teamName) => (
                <div key={teamName} onClick={() => showHideSidebar((o) => !o)}>
                  <Text weight={500} size="md">
                    {teamName}
                  </Text>
                  <Link to={`${to}/members/${teamName}`}>
                    <Text size="sm" color="dimmed" mt="sm" weight={400}>
                      Members
                    </Text>
                  </Link>
                  <Link to={`${to}/books/${teamName}`}>
                    <Text size="sm" color="dimmed" mt="sm" weight={400}>
                      Books
                    </Text>
                  </Link>
                  <Divider my="sm" />
                </div>
              ))
            ) : (
              <Text>You are currently in no team</Text>
            )}
          </Accordion.Item>
        </Accordion>
      ) : (
        <Skeleton height={40} mt={6} radius="sm" />
      )}
    </>
  );
}
