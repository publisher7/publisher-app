import './SideBarLinks.css';
import { FilePlus, Notes, Users, CirclePlus, BellRinging, Book } from 'tabler-icons-react';
import MainLink from './MainLink';
import UserTeamsLink from './UserTeamsLink';
import NotificationsLink from '../../Notifications/NotificationsLink';
import { useContext } from 'react';
import AppContext from '../../../providers/AppContext';

export function MainLinks({ showHideSidebar }) {
  const { userData } = useContext(AppContext);

  const adminLinks = [
    { icon: <CirclePlus size={22} />, color: 'purple', label: 'New Team', to: 'create-team' },
  ];

  const employeeLinks = [
    { icon: <Notes size={22} />, color: 'teal', label: 'Items to Review', to: 'my-reviews' },
    {
      icon: <BellRinging size={22} />,
      color: 'grape',
      label: 'Notifications',
      to: 'notifications',
    },
    {
      icon: <Users size={22} />,
      color: 'violet',
      label: 'My Teams',
      to: 'my-teams',
    },
  ];

  const authorLinks = [
    { icon: <FilePlus size={22} />, color: 'blue', label: 'Send new script', to: 'create' },
    { icon: <Book size={22} />, color: 'orange', label: 'My books', to: 'author-books' },
    {
      icon: <BellRinging size={22} />,
      color: 'grape',
      label: 'Notifications',
      to: 'notifications',
    },
  ];

  return (
    <>
      {userData && (
        <>
          {userData.role === 'admin' &&
            adminLinks.map((link) => {
              return <MainLink {...link} key={link.label} showHideSidebar={showHideSidebar} />;
            })}
          {userData.role !== 'author' &&
            employeeLinks.map((link) => {
              if (link.label === 'My Teams') {
                return (
                  <UserTeamsLink
                    {...link}
                    label={`${userData.role === 'admin' ? 'All Teams' : link.label}`}
                    key={link.label}
                    showHideSidebar={showHideSidebar}
                  />
                );
              }

              if (link.label === 'Notifications') {
                return (
                  <NotificationsLink
                    link={link}
                    key={link.label}
                    showHideSidebar={showHideSidebar}
                  />
                );
              }
              return <MainLink {...link} key={link.label} showHideSidebar={showHideSidebar} />;
            })}
          {userData.role === 'author' &&
            authorLinks.map((link) => {
              return link.label === 'Notifications' ? (
                <NotificationsLink link={link} key={link.label} showHideSidebar={showHideSidebar} />
              ) : (
                <MainLink
                  key={link.label}
                  {...link}
                  to={link.to === 'author-books' ? link.to + '/' + userData.username : link.to}
                  showHideSidebar={showHideSidebar}
                />
              );
            })}
        </>
      )}
    </>
  );
}
