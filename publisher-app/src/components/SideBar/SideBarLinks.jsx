import React from 'react';
import { Messages, Notebook, Notes, AddressBook } from 'tabler-icons-react';
import { ThemeIcon, UnstyledButton, Group, Text } from '@mantine/core';
import { Link } from 'react-router-dom';

function MainLink({ icon, color, label, to }) {
  return (
    <>
      <Link to={to}>
        <UnstyledButton
          sx={(theme) => ({
            display: 'block',
            width: '100%',
            padding: theme.spacing.xs,
            borderRadius: theme.radius.sm,
            color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,

            '&:hover': {
              backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
            },
          })}
        >
          <Group>
            <ThemeIcon color={color} variant="light">
              {icon}
            </ThemeIcon>

            <Text size="sm">{label}</Text>
          </Group>
        </UnstyledButton>
      </Link>
    </>
  );
}

const data = [
  { icon: <Notebook size={16} />, color: 'blue', label: 'All Items', to: 'all' },
  { icon: <Notes size={16} />, color: 'teal', label: 'My Items', to: 'card' },
  { icon: <AddressBook size={16} />, color: 'violet', label: 'Team Members', to: 'home' },
  { icon: <Messages size={16} />, color: 'grape', label: 'Inbox', to: 'home' },
];

export function MainLinks() {
  const links = data.map((link) => <MainLink {...link} key={link.label} />);
  return <div>{links}</div>;
}
