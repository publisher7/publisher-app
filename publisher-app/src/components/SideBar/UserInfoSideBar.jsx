import React, { useContext } from 'react';
import { UnstyledButton, Group, Avatar, Text, Box, useMantineTheme, Skeleton } from '@mantine/core';
import AppContext from '../../providers/AppContext';
import { Link } from 'react-router-dom';

export function UserInfoSideBar({ showHideSidebar }) {
  const theme = useMantineTheme();
  const { userData } = useContext(AppContext);

  return (
    <>
      <Link to={`my-profile`} onClick={() => showHideSidebar((o) => !o)}>
        <Box
          sx={{
            paddingTop: theme.spacing.sm,
            borderTop: `1px solid ${
              theme.colorScheme === 'dark' ? theme.colors.dark[4] : theme.colors.gray[2]
            }`,
          }}
        >
          <UnstyledButton
            sx={{
              display: 'block',
              width: '100%',
              padding: theme.spacing.xs,
              borderRadius: theme.radius.sm,
              color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,

              '&:hover': {
                backgroundColor:
                  theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
              },
            }}
          >
            <Group position="center">
              {userData ? (
                <Avatar src={userData.avatarUrl} size="lg" radius="md" />
              ) : (
                <Skeleton height={85} width={85} radius="md" />
              )}
              {userData ? (
                <Box sx={{ flex: 1 }}>
                  <Text align="center" size="sm" weight={500}>
                    {`${userData.firstName} ${userData.lastName}`}
                  </Text>
                  <Text align="center" color="dimmed" size="xs">
                    {`${userData.email}`}
                  </Text>
                </Box>
              ) : (
                <Skeleton height={45} width="100%" />
              )}
            </Group>
          </UnstyledButton>
        </Box>
      </Link>
    </>
  );
}
