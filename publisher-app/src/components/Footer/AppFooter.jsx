import { ActionIcon, Footer, Group, Text } from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import React from 'react';
import { BrandGitlab } from 'tabler-icons-react';

export default function AppFooter() {
  const smallScreen = useMediaQuery('(max-width: 600px)');

  return (
    <Footer height={60} p="md" m={0}>
      <Group position="apart" mx="md">
        <Text color="dimmed" size="sm">
          {`© 2022 The Publisher.`}
        </Text>
        {!smallScreen && (
          <Text color="dimmed" size="sm">
            {`Special credits to Nadezhda Hristova and Stoyan Peshev. :)`}
          </Text>
        )}
        <Group position="right" noWrap>
          <ActionIcon
            component={'a'}
            href="https://gitlab.com/publisher7/publisher-app/-/tree/main/publisher-app"
            size="lg"
            target="_blank"
            rel="noreferrer"
          >
            <BrandGitlab size={18} />
          </ActionIcon>
          {/* <ActionIcon size="lg">
            < size={18} />
          </ActionIcon>
          <ActionIcon size="lg">
            <BrandInstagram size={18} />
          </ActionIcon> */}
        </Group>
      </Group>
    </Footer>
  );
}
