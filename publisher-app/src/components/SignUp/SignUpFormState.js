export const initialFormValues = {
  firstName: '',
  lastName: '',
  username: '',
  phoneNumber: '',
  password: '',
  confirmPassword: '',
  email: '',
  code: '',
};

export const formValidations = {
  email: (value) => (/^\S+@\S+$/.test(value) ? null : 'Invalid email address.'),
  firstName: (value) =>
    value.length > 2 && value.length <= 20
      ? null
      : 'First name should be between 2 and 20 characters.',
  lastName: (value) =>
    value.length > 2 && value.length <= 20
      ? null
      : 'Last name should be between 2 and 20 characters.',
  username: (value) =>
    value.length > 2 && value.length <= 20
      ? null
      : 'Username should be between 2 and 20 characters.',
  phoneNumber: (value) => (/^[+]\d{12}$/.test(value) ? null : 'Please enter a valid phone number'),
  password: (value) =>
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/.test(value)
      ? null
      : 'Password must contain at least one number, one lower, one upper case, one special character',
  confirmPassword: (value, values) =>
    value !== values.password ? 'Passwords did not match' : null,
  code: (value) =>
    value === 'PubEmp' || value === 'PubAdm' || value === '' ? null : 'Invalid code',
};

export const textInputs = [
  { label: 'First Name', proxy: '', prop: 'firstName' },
  { label: 'Last Name', proxy: '', prop: 'lastName' },
  { label: 'Username', proxy: '', prop: 'username' },
  { label: 'Email', proxy: 'example@mail.com', prop: 'email' },
  { label: 'Phone Number', proxy: '+359xxxxxxxxx', prop: 'phoneNumber' },
  { label: 'Code', proxy: 'employee code', prop: 'code' },
];
