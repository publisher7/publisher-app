import { TextInput, PasswordInput, Group, Modal, Anchor } from '@mantine/core';
import { useForm } from '@mantine/form';
import { addAdditionalUserInfo, getUserByUsername } from '../../services/user.services';
import { initialFormValues, formValidations, textInputs } from './SignUpFormState';
import { useState } from 'react';
import { signup } from '../../services/auth.services';
import { ButtonSecondary } from '../SharedComponents/Buttons';
import AlertDanger from '../SharedComponents/AlertDanger';
import { successNotification } from '../../common/helpers';

export default function SignUp({ openSignUp, setOpenSignUp, changeSignForms, setIsLogged }) {
  const [error, setError] = useState('');
  const form = useForm({
    initialValues: { ...initialFormValues },
    validate: { ...formValidations },
  });

  const firebaseValidation = (values) => {
    setError('');
    getUserByUsername(values.username)
      .then((snapshot) => {
        if (snapshot.exists()) {
          return setError(`User @${values.username} already exists!`);
        }

        return signup(values.email, values.password)
          .then((u) => {
            setIsLogged(true);
            addAdditionalUserInfo(
              values.username,
              u.user.uid,
              u.user.email,
              values.firstName,
              values.lastName,
              values.phoneNumber,
              values.code,
            )
              .then(() => {
                setOpenSignUp(false);
                form.setValues(initialFormValues);
                successNotification('You are now logged in!', 'Registration successful!');
              })
              .catch(console.error);
          })
          .catch((e) => {
            if (e.message.includes(`email-already-in-use`)) {
              setError(`Email ${values.email} has already been registered!`);
            }
          });
      })
      .catch((err) => setError(`${err.message}`));
  };

  const inputsElements = textInputs.map((input, key) => (
    <TextInput
      required={input.label === 'Code' ? false : true}
      key={input.label}
      label={input.label}
      placeholder={input.proxy}
      mt="sm"
      autoComplete="off"
      sx={{ maxWidth: key < 2 ? '45%' : '100%' }}
      {...form.getInputProps(`${input.prop}`)}
    />
  ));

  return (
    <Modal
      centered
      opened={openSignUp}
      onClose={() => setOpenSignUp(false)}
      title="Register"
      size="lg"
    >
      <form onSubmit={form.onSubmit((values) => firebaseValidation(values))} autoComplete="off">
        <Group position="apart" grow={true}>
          {[...inputsElements.slice(0, 2)]}
        </Group>
        {[...inputsElements.slice(2)]}
        <PasswordInput
          autoComplete="new-password"
          required
          label="Password"
          placeholder="Password"
          mt="sm"
          {...form.getInputProps('password')}
        />
        <PasswordInput
          autoComplete="new-password"
          required
          mt="sm"
          label="Confirm password"
          placeholder="Confirm password"
          {...form.getInputProps('confirmPassword')}
        />
        {error && <AlertDanger message={error} />}
        <Group position="apart" mt="xl" spacing="xl">
          <Anchor onClick={changeSignForms}>Have an account? Login</Anchor>
          <ButtonSecondary text="Submit" type="submit" />
        </Group>
      </form>
    </Modal>
  );
}
