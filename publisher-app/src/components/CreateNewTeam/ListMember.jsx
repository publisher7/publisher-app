import { Checkbox, Group, Avatar, Text } from '@mantine/core';

export default function ListMember({ data, selected }) {
  return (
    <Group noWrap onChange={() => {}}>
      <Avatar src={data.avatarUrl} radius="md" size="md" />
      <div style={{ flex: 1 }}>
        <Text size="md" weight={500}>
          {`${data.firstName} ${data.lastName}`}
        </Text>
        <Text size="sm" color="dimmed" weight={400}>
          {data.email}
        </Text>
      </div>
      <Checkbox
        checked={selected}
        onChange={() => {}}
        tabIndex={-1}
        sx={{ pointerEvents: 'none' }}
      />
    </Group>
  );
}
