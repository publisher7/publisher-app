import { forwardRef } from 'react';
import { Container, TextInput, MultiSelect, Group, Avatar, Text } from '@mantine/core';

export default function SelectOwners({ smallScreen, chooseOwners, setChooseOwners, ownersData }) {
  const userInfoItem = forwardRef(({ image, label, description, ...others }, ref) => (
    <div ref={ref} {...others}>
      <Group noWrap>
        <Avatar src={image} />

        <div>
          <Text>{label}</Text>
          <Text size="xs" color="dimmed">
            {description}
          </Text>
        </div>
      </Group>
    </div>
  ));

  return (
    <Container size="lg" sx={{ margin: '0, auto', width: `${smallScreen ? '100%' : '50%'}` }}>
      <MultiSelect
        value={chooseOwners}
        onChange={setChooseOwners}
        data={ownersData}
        itemComponent={userInfoItem}
        label="Select owner/s of the team."
        placeholder="Pick all that you like"
        nothingFound="Nothing found"
        maxDropdownHeight={200}
        required
        clearable
        searchable
      />
    </Container>
  );
}
