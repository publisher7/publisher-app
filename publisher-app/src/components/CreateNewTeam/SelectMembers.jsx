import { TransferList, Container } from '@mantine/core';
import ListMember from './ListMember';

export default function SelectMembers({ data, setData }) {
  const ItemComponent = ({ data, selected }) => <ListMember data={data} selected={selected} />;
  return (
    <>
      {data && (
        <Container fluid={true} mt="xl">
          <TransferList
            value={data}
            onChange={setData}
            searchPlaceholder="Search employees..."
            nothingFound="Select employee"
            titles={['All Employees', 'Employees to assign']}
            listHeight={300}
            breakpoint="md"
            itemComponent={ItemComponent}
            filter={(query, item) =>
              item.email.toLowerCase().includes(query.toLowerCase().trim()) ||
              item.firstName.toLowerCase().includes(query.toLowerCase().trim()) ||
              item.lastName.toLowerCase().includes(query.toLowerCase().trim())
            }
          />
        </Container>
      )}
    </>
  );
}
