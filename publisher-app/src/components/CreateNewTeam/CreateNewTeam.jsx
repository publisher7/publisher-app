import { useEffect, useState } from 'react';
import Loader from '../SharedComponents/Loader/Loader';
import { fromUsersDocument, getAllUsers } from '../../services/user.services';
import SelectMembers from './SelectMembers';
import { useMediaQuery } from '@mantine/hooks';
import { Container, Group, TextInput } from '@mantine/core';
import { ButtonPrimary } from '../SharedComponents/Buttons';
import SelectOwners from './SelectOwners';
import AlertDanger from '../SharedComponents/AlertDanger';
import { createNewTeam } from '../../services/team.services';
import { successNotification } from '../../common/helpers';

export default function CreateNewTeam() {
  const [sendData, setSendData] = useState(false);
  const smallScreen = useMediaQuery('(max-width: 550px)');
  const [users, setUsers] = useState([null, []]);
  const [teamName, setTeamName] = useState('');
  const [chooseOwners, setChooseOwners] = useState([]);
  const [ownersData, setOwnersData] = useState([]);
  const [error, setError] = useState('');

  useEffect(() => {
    getAllUsers().then((snapshot) => {
      const users = fromUsersDocument(snapshot).filter((user) => user.role === 'employee');

      setUsers([users.map((user) => ({ ...user, value: user.uid })), []]);
      setOwnersData(
        users.map((user) => ({
          value: user.username,
          label: `${user.firstName} ${user.lastName}`,
          image: user.avatarUrl,
          description: user.email,
        })),
      );
    });
  }, [sendData]);

  const createTeam = (e) => {
    e.preventDefault();

    if (teamName === '') {
      return setError('Please enter a team name!');
    }

    if (chooseOwners.length === 0) {
      return setError('Please choose at least one team owner!');
    }

    if (users[1].length === 0) {
      return setError('Please choose at least one employee!');
    }

    setError('');
    const membersNames = users[1].map((u) => u.username);
    createNewTeam(teamName, chooseOwners, membersNames)
      .then((result) => {
        if (result === 'Error') {
          return setError('Team with the current name already exists!');
        }
        setTeamName('');
        setChooseOwners([]);
        setError('');
        setUsers([null, []]);
        setTimeout(() => {
          setSendData((b) => !b);
          successNotification('Team created successfully!');
        }, 2000);
      })
      .catch((err) => setError(err.message));
  };

  return (
    <>
      {users[0] ? (
        <Container fluid={true}>
          <Group grow={true} direction={smallScreen ? 'column' : 'row'}>
            <Container size="lg" mx={0}>
              <TextInput
                required
                label="Team Name"
                placeholder="Enter name here.."
                autoComplete="off"
                value={teamName}
                onChange={(e) => setTeamName(e.target.value)}
              />
            </Container>
            <SelectOwners
              smallScreen={smallScreen}
              chooseOwners={chooseOwners}
              setChooseOwners={setChooseOwners}
              ownersData={ownersData}
            />
          </Group>
          <SelectMembers data={users} setData={setUsers} />
          {error && <AlertDanger message={error} />}
          <Group position="center" mt="xl">
            <ButtonPrimary text="Create Team" onClick={createTeam} />
          </Group>
        </Container>
      ) : (
        <Loader />
      )}
    </>
  );
}
