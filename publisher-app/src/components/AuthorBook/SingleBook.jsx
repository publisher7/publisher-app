import { useState, useEffect } from 'react';
import { Container, Grid } from '@mantine/core';
import BookStepper from './BookStepper/BookStepper';
import { getLiveSingleBook } from '../../services/book.services';
import { useMediaQuery } from '@mantine/hooks';
import { useNavigate, useParams } from 'react-router';
import BookStepperMobile from './BookStepper/BookStepperMobile';
import SingleItemView from '../SingleItemView/SingleItemView';

export default function SingleBook() {
  const [bookData, setBookData] = useState(null);
  const midscreen = useMediaQuery('(max-width: 768px)');
  const { bookId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    const unsubscribe = getLiveSingleBook(bookId, (snapshot) => {
      if (!snapshot.val()) {
        navigate('/error');
      } else {
        setBookData(snapshot.val());
      }
    });

    return () => unsubscribe();
  }, [bookId]);

  return (
    <>
      {bookData && (
        <Container fluid={true} mt="xl">
          <Grid columns={8} justify="center">
            {midscreen && <BookStepperMobile active={bookData.currentTeam} />}
            <Grid.Col span={midscreen ? 0 : 2}>
              {!midscreen && <BookStepper active={bookData.currentTeam} />}
            </Grid.Col>
            <Grid.Col span={midscreen ? 8 : 6}>
              <SingleItemView authorBook={{ id: bookData.id, currentTeam: bookData.currentTeam }} />
            </Grid.Col>
          </Grid>
        </Container>
      )}
    </>
  );
}
