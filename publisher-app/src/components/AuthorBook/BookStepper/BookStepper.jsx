import { Stepper, Title } from '@mantine/core';
import { stepperData } from './BookStepperData';
import { CircleCheck } from 'tabler-icons-react';

export default function BookStepper({ active }) {
  return (
    <>
      <Title order={3}>Progress</Title>
      <Stepper
        orientation="vertical"
        mt="lg"
        active={active}
        color="yellow"
        breakpoint="sm"
        completedIcon={<CircleCheck />}
      >
        {stepperData.map((step, index) => (
          <Stepper.Step {...step} loading={active === index ? true : false} key={step.label} />
        ))}
      </Stepper>
    </>
  );
}
