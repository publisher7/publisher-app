import { Group, Text, Stepper } from '@mantine/core';
import { CircleCheck } from 'tabler-icons-react';
import { teamNames } from '../../../common/helpers';
import { stepperData } from './BookStepperData';

export default function BookStepperMobile({ active }) {
  const currentTeam = stepperData[active].label;
  return (
    <Group align="center" position="center">
      <Text size="lg" weight={500}>
        Phase:
      </Text>
      <Stepper
        orientation="vertical"
        active={active}
        color="orange"
        breakpoint="sm"
        completedIcon={<CircleCheck />}
        sx={{ height: '100%', display: 'block' }}
        size="lg"
      >
        <Stepper.Step label={currentTeam} description="" loading={true} />
      </Stepper>
    </Group>
  );
}
