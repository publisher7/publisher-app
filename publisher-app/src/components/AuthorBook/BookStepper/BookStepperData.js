import { Directions, Book, FileText, Pencil, Writing, MailForward } from 'tabler-icons-react';

export const stepperData = [
  {
    label: 'Submit a draft',
    description: '',
    icon: <FileText size={20} />,
  },
  {
    label: 'Script review',
    description: '',
    icon: <MailForward size={20} />,
  },
  {
    label: 'Proposal and contract',
    description: '',
    icon: <Writing size={20} />,
  },
  {
    label: 'Copy-editing & \n Proof-reading',
    description: '',
    icon: <Pencil size={20} />,
  },
  {
    label: 'Artwork & Printing',
    description: '',
    icon: <Book size={20} />,
  },
  {
    label: 'Marketing & Distribution',
    description: '',
    icon: <Directions size={20} />,
  },
];
