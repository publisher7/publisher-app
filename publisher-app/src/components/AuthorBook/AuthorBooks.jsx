import { useContext, useEffect, useState } from 'react';
import { getLiveAuthorBooks, getBookById } from '../../services/book.services';
import { Text, SimpleGrid } from '@mantine/core';
import AppContext from '../../providers/AppContext';
import Loader from '../SharedComponents/Loader/Loader';
import { SingleCard } from '../AllItemsView/SIngleCard';

export default function AuthorBooks() {
  const [authorBooks, setAuthorBooks] = useState(null);
  const { userData } = useContext(AppContext);

  useEffect(() => {
    const unsubscribe = getLiveAuthorBooks(userData?.username, (snapshot) => {
      if (userData) {
        if (!snapshot.exists()) {
          return setAuthorBooks([]);
        }
        const bookIds = Object.keys(snapshot.val());

        Promise.all(bookIds.map((id) => getBookById(id)))
          .then(setAuthorBooks)
          .catch((err) => console.log(err));
      }
    });

    return () => unsubscribe();
  }, [userData]);

  return (
    <>
      {authorBooks ? (
        <SimpleGrid
          cols={5}
          spacing="sm"
          breakpoints={[
            { maxWidth: 1850, cols: 4, spacing: 'sm' },
            { maxWidth: 1466, cols: 3, spacing: 'sm' },
            { maxWidth: 1220, cols: 2, spacing: 'sm' },
            { maxWidth: 600, cols: 1, spacing: 'sm' },
          ]}
        >
          {authorBooks.length > 0 ? (
            authorBooks.map((book) => <SingleCard book={book} key={book.id} />)
          ) : (
            <Text weight={500} size="xl" align="center">
              No books submitted
            </Text>
          )}
        </SimpleGrid>
      ) : (
        <Loader />
      )}
    </>
  );
}
