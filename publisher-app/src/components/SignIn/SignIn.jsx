import { useState, useCallback, useContext } from 'react';
import { useForm } from '@mantine/form';
import { TextInput, PasswordInput, Group, Modal, Anchor } from '@mantine/core';
import SignUp from '../SignUp/SignUp';
import { login } from '../../services/auth.services';
import { changeLoggedStatus, getUserData } from '../../services/user.services';
import AppContext from '../../providers/AppContext';
import { ButtonSecondary } from '../SharedComponents/Buttons';
import AlertDanger from '../SharedComponents/AlertDanger';

export default function SignIn({ openSignIn, setOpenSignIn, setIsLogged }) {
  const initialFormValues = {
    email: '',
    password: '',
  };
  const form = useForm({
    initialValues: { ...initialFormValues },
  });
  const [openSignUp, setOpenSignUp] = useState(false);
  const [error, setError] = useState('');
  const { setContext } = useContext(AppContext);
  const changeSignForms = useCallback(() => {
    setOpenSignIn((o) => !o);
    setOpenSignUp((o) => !o);
  }, [setOpenSignUp, setOpenSignIn]);

  const handleLogin = ({ email, password }) => {
    setError('');
    login(email, password)
      .then(async (u) => {
        const snapshot = await getUserData(u.user.uid);
        if (snapshot.exists()) {
          const userData = snapshot.val()[Object.keys(snapshot.val())[0]];
          setIsLogged(true);
          setContext({
            user: u.user,
            userData,
          });
          changeLoggedStatus(userData.username);
        }
      })
      .then(() => {
        setOpenSignIn(false);
        form.setValues({ ...initialFormValues });
      })
      .catch((err) => {
        if (err.message.includes('invalid-email') || err.message.includes('user-not-found')) {
          setError('No user with the current email registered!');
        }

        if (err.message.includes('wrong-password')) {
          setError('You have entered wrong password.');
        }

        if (err.message.includes('too-many-requests')) {
          setError('Too many requests. Try again in a few moments.');
        }
      });
  };

  return (
    <>
      <Modal
        centered
        opened={openSignIn}
        onClose={() => setOpenSignIn(false)}
        title="Log in"
        size="md"
      >
        <form onSubmit={form.onSubmit((values) => handleLogin(values))} autoComplete="off">
          <TextInput
            required
            label="Email address"
            placeholder="example@mail.com"
            mt="sm"
            autoComplete="off"
            {...form.getInputProps(`email`)}
          />
          <PasswordInput
            autoComplete="new-password"
            required
            label="Password"
            placeholder="Password"
            mt="sm"
            {...form.getInputProps('password')}
          />

          {error && <AlertDanger message={error} />}
          <Group position="apart" mt="xl" spacing="xl">
            <Anchor onClick={changeSignForms}>Want to join us? Register</Anchor>
            <ButtonSecondary text="Login" type="submit" />
          </Group>
        </form>
      </Modal>
      <SignUp
        openSignUp={openSignUp}
        setOpenSignUp={setOpenSignUp}
        changeSignForms={changeSignForms}
        setIsLogged={setIsLogged}
      />
    </>
  );
}
