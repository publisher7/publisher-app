import { Avatar, Group, Text } from '@mantine/core';
import { forwardRef } from 'react';

export const userInfoItem = forwardRef(({ image, label, description, ...others }, ref) => (
  <div ref={ref} {...others}>
    <Group noWrap>
      <Avatar src={image} />
      <div>
        <Text>{label}</Text>
        <Text size="xs" color="dimmed">
          {description}
        </Text>
      </div>
    </Group>
  </div>
));
