import React, { useEffect, useState } from 'react';
import { createStyles, Avatar, Text, Group, Skeleton } from '@mantine/core';
import { PhoneCall, At } from 'tabler-icons-react';
import { fromUsersDocument, getUserByUsername, getUserData } from '../../services/user.services';

const useStyles = createStyles((theme) => ({
  icon: {
    color: theme.colorScheme === 'dark' ? theme.colors.dark[3] : theme.colors.gray[5],
    // maxHeight: '20px',
  },
  main: {
    // maxHeight: '50px',
    position: 'end',
  },
  name: {
    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
  },
}));

export function UserInfoIcons({ authorName }) {
  const { classes } = useStyles();
  const [authorData, setAuthorData] = useState(null);

  useEffect(() => {
    getUserByUsername(authorName)
      .then((snapshot) => setAuthorData(snapshot.val()))
      .catch((err) => console.log(err));
  }, [authorName]);

  return (
    <>
      {authorData ? (
        <Group noWrap className={classes.main}>
          <Avatar src={authorData.avatarUrl} size={84} radius="md" />
          <div>
            <Text size="xs" sx={{ textTransform: 'uppercase' }} weight={700} color="dimmed">
              Author
            </Text>

            <Text size="lg" weight={500} className={classes.name}>
              {`${authorData.firstName} ${authorData.lastName}`}
            </Text>

            <Group noWrap spacing={10} mt={3} className={classes.main}>
              <At size={16} className={classes.icon} />
              <Text size="xs" color="dimmed">
                {authorData.email}
              </Text>
            </Group>

            <Group noWrap spacing={10} mt={5}>
              <PhoneCall size={16} className={classes.icon} />
              <Text size="xs" color="dimmed">
                {authorData.phoneNumber}
              </Text>
            </Group>
          </div>
        </Group>
      ) : (
        <Group noWrap className={classes.main}>
          <Skeleton height={80} width={80} radius="md" />
          <div>
            <Skeleton height={15} width={155} radius="sm" mt={6} />
            <Skeleton height={20} width={155} radius="sm" mt={12} />
            <Skeleton height={12} width={155} radius="sm" mt={6} />
            <Skeleton height={12} width={155} radius="sm" mt={6} />
          </div>
        </Group>
      )}
    </>
  );
}
