import { Button, Card, createStyles } from '@mantine/core';
import React, { useContext, useEffect, useState } from 'react';
import AppContext from '../../providers/AppContext';
import { getLiveSingleBook, updateThirdTeamReviewer } from '../../services/book.services';
import { addToUserReviewing } from '../../services/user.services';
import DownloadableLink from './DowloadableLink';
import { singleItemStyles } from './SingleItemViewStyles';
import { UploadPopover } from './UploadPopover';

const useStyles = createStyles((theme) => singleItemStyles(theme));

export default function ThirdTeamUser({ book, isAuthor, teamName }) {
  const [visibleAssign, setVisibleAssign] = useState(false);
  const { userData } = useContext(AppContext);
  const { classes } = useStyles();

  const handleAssign = () => {
    updateThirdTeamReviewer(book.id, userData.username);
    addToUserReviewing(userData.username);
  };

  useEffect(() => {
    const unsubscribe = getLiveSingleBook(book.id, (snapshot) => {
      const book = snapshot.val();

      if (!book?.team3?.reviewer) {
        setVisibleAssign(true);
      } else {
        setVisibleAssign(false);
      }
    });
    return () => unsubscribe();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [visibleAssign]);

  return (
    <>
      {book.currentTeam !== 1 && book.currentTeam !== 2 && (
        <Card.Section mt={20} position="center">
          {book?.team3?.reviewer === userData.username && !book?.team3?.files && (
            <UploadPopover book={book} isAuthor={isAuthor} teamName={teamName} />
          )}
          <Card.Section className={classes.sectionFinalScript} position="center">
            <DownloadableLink files={book?.team3?.files} title={'Final script here'} position={'left'} />
          </Card.Section>
          {visibleAssign && !isAuthor && (
            <Button onClick={handleAssign} size="12px">
              Assign to myself
            </Button>
          )}
        </Card.Section>
      )}
    </>
  );
}
