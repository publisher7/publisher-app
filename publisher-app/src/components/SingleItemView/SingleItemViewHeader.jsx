import { Card, Text, Group, Badge, createStyles, Title, Container } from '@mantine/core';
import { Link } from 'react-router-dom';
import { ButtonSecondary } from '../SharedComponents/Buttons';
import { singleItemStyles } from './SingleItemViewStyles';
import { Backspace } from 'tabler-icons-react';
import { UserInfoIcons } from './testauthor';
import { useMediaQuery } from '@mantine/hooks';

const useStyles = createStyles((theme) => singleItemStyles(theme));

export const BookBasicInfo = ({ book, teamName, userData }) => {
  const { classes } = useStyles();
  const smallScreen = useMediaQuery('(max-width: 550px)');

  const linkTo =
    userData.role === 'author'
      ? `/author-books/${userData.username}`
      : teamName
      ? `/my-teams/books/${teamName}`
      : `/my-reviews/`;

  return (
    <>
      <Group className={classes.section} position={smallScreen ? 'center' : 'apart'} noWrap={false}>
        <Link to={linkTo} className={'link'}>
          <ButtonSecondary
            text={userData.role === 'author' || !teamName ? 'My Books' : 'Team Books'}
            icon={<Backspace />}
          />
        </Link>
        <Group position="center">
          <Container size={'xs'}>
            <Title align="center">{book.title}</Title>
          </Container>
        </Group>
        <UserInfoIcons authorName={book.author} />
      </Group>
      <Card.Section className={classes.section} mt="md">
        <Text size="lg" weight={500}>
          Resume
        </Text>
        <Group position="left">
          {book.genre?.map((el) => (
            <Badge size="sm" key={book.genre + el}>
              {`Genre - ${el}`}
            </Badge>
          ))}
        </Group>

        <Text size="sm" mt="xs">
          {book.resume}
        </Text>
      </Card.Section>
    </>
  );
};
