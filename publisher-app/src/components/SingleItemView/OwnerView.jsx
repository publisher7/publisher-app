import { Card, createStyles, Group, Modal, MultiSelect } from '@mantine/core';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import {
  deleteBook,
  updateFirstReviewerName,
  updateFirstReviewerStatus,
  updateSecondReviewerName,
  updateSecondReviewerStatus,
} from '../../services/book.services';
import { sendNotification } from '../../services/notification.services';
import { addToUserReviewing, fromUsersDocument, getAllUsers } from '../../services/user.services';
import { ButtonDanger, ButtonSecondary } from '../SharedComponents/Buttons';
import { singleItemStyles } from './SingleItemViewStyles';
import { userInfoItem } from './UserInfoItem';

const useStyles = createStyles((theme) => singleItemStyles(theme));

export default function OwnerView({ book, teamName, bookId, teamMembers }) {
  const { classes } = useStyles();
  const [openConfirmModal, setOpenConfirmModal] = useState(false);
  const navigate = useNavigate();

  const [firstReviewerValue, setFirstReviewerValue] = useState([]);
  const [secondReviewerValue, setSecondReviewerValue] = useState([]);
  const [firstReviewerData, setFirstReviewerData] = useState([]);
  const [secondReviewerData, setSecondReviewerData] = useState([]);

  useEffect(() => {
    getAllUsers().then((snapshot) => {
      const users = fromUsersDocument(snapshot);

      if (book.title?.length > 0) {
        setFirstReviewerData(
          users
            .filter((user) => Object.keys(teamMembers).includes(user.username))
            .filter((user) => user.username !== book.reviews.secondReviewer.name)
            .map((user) => ({
              value: user.username,
              label: `${user.firstName} ${user.lastName}`,
              image: user.avatarUrl,
              description: user.email,
              key: user.username,
            })),
        );
      }
      if (book.title?.length > 0) {
        setSecondReviewerData(
          users
            .filter((user) => Object.keys(teamMembers).includes(user.username))
            .filter((user) => user.username !== book.reviews.firstReviewer.name)
            .map((user) => ({
              value: user.username,
              label: `${user.firstName} ${user.lastName}`,
              image: user.avatarUrl,
              description: user.email,
              key: user.username,
            })),
        );
      }
    });
  }, [teamMembers, book]);

  const handleDelete = () => {
    deleteBook(book.id, teamName);
    navigate(`/my-teams/books/${teamName}`, { replace: true });
  };
  const handleFirstReviewer = (value) => {
    if (book.reviews.firstReviewer.name && book.reviews.firstReviewer.status) {
      updateFirstReviewerStatus(bookId, '');
    }

    setFirstReviewerValue(value);
    if (value.length === 0) {
      return updateFirstReviewerName(bookId, '');
    }
    addToUserReviewing(value);
    sendNotification([value], `You have been assigned to review ${book.title} in ${teamName}`);

    return updateFirstReviewerName(bookId, value[0]);
  };
  const handleSecondReviewer = (value) => {
    if (book.reviews.secondReviewer.name && book.reviews.secondReviewer.status) {
      updateSecondReviewerStatus(bookId, '');
    }
    setSecondReviewerValue(value);
    if (value.length === 0) {
      return updateSecondReviewerName(bookId, '');
    }
    addToUserReviewing(value);
    sendNotification([value], `You have been assigned to review ${book.title} in ${teamName}`);

    return updateSecondReviewerName(bookId, value[0]);
  };

  return (
    <Card.Section className={classes.dropdown} position="center">
      <MultiSelect
        value={firstReviewerValue}
        onChange={(value) => handleFirstReviewer(value)}
        data={firstReviewerData}
        itemComponent={userInfoItem}
        label="Select first reviewer."
        placeholder="Pick all that you like"
        nothingFound="First reviewer is already assigned"
        maxDropdownHeight={200}
        required
        clearable
        searchable
        maxSelectedValues={1}
      />
      <MultiSelect
        value={secondReviewerValue}
        onChange={(value) => handleSecondReviewer(value)}
        data={secondReviewerData}
        itemComponent={userInfoItem}
        label="Select second reviewer."
        placeholder="Pick all that you like"
        nothingFound="Second reviewer is already assigned"
        maxDropdownHeight={200}
        required
        clearable
        searchable
        maxSelectedValues={1}
      />
      <br />
      <Group position="apart">
        <ButtonDanger text="Delete" onClick={setOpenConfirmModal} />

        <Modal
          size="sx"
          centered
          opened={openConfirmModal}
          onClose={() => setOpenConfirmModal(false)}
          title="Remove current member?"
        >
          {' '}
          <Group position="center" mt="md" spacing="xl">
            <ButtonDanger text="Confirm" onClick={handleDelete} />
            <ButtonSecondary text="Cancel" onClick={() => setOpenConfirmModal(false)} />
          </Group>
        </Modal>
        <ButtonSecondary
          text="Submit Reviewer"
          onClick={() => {
            setFirstReviewerValue(null);
            setSecondReviewerValue(null);
          }}
        />
      </Group>
    </Card.Section>
  );
}
