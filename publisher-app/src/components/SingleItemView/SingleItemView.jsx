import React, { useContext, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router';
import { useParams } from 'react-router-dom';
import { Card, createStyles, Select, SimpleGrid, Loader, Text } from '@mantine/core';
import {
  getLiveSingleBook,
  getLiveTeamMembers,
  updateBookStatus,
  updateFirstReviewerStatus,
  updateSecondReviewerStatus,
} from '../../services/book.services';
import AppContext from '../../providers/AppContext';
import { sendNotification } from '../../services/notification.services';
import { warningNotification, teamNames } from '../../common/helpers';
import { singleItemStyles } from './SingleItemViewStyles';
import { BookBasicInfo } from './SingleItemViewHeader';
import OwnerView from './OwnerView';
import CurrentReviewers from './CurrentReviewers';
import DownloadableLink from './DowloadableLink';
import AddComment from './AddComment';
import SecondTeamAuthorView from './SecondTeamAuthorView';
import SecondTeamUser from './SecondTeamUser';
import ThirdTeamUser from './ThirdTeamUser';
import FourthTeamUser from './FourthTeamUser';
import { UploadPopover } from './UploadPopover';

const useStyles = createStyles((theme) => singleItemStyles(theme));

export default function SingleItemView({ authorBook = null }) {
  const { bookId } = useParams();
  const id = authorBook ? authorBook.id : bookId;
  const sharedData = useLocation();
  const teamName = authorBook ? teamNames[authorBook?.currentTeam - 1] : sharedData.state.teamName;
  const { userData } = useContext(AppContext);
  const navigate = useNavigate();
  const { classes } = useStyles();
  const [book, setBook] = useState([]);
  const [teamMembers, setTeamMembers] = useState([]);
  // eslint-disable-next-line no-unused-vars
  const [_, setTeamOwner] = useState([]);
  const [isOwner, setIsOwner] = useState(false);
  const [commentValue, setCommentValue] = useState('');
  const [currentStatus, setCurrentStatus] = useState([]);
  const [isAuthor, setIsAuthor] = useState(false);
  useEffect(() => {
    const unsubscribe = getLiveSingleBook(id, (snapshot) => {
      const book = snapshot.val();
      setBook(book);

      return () => unsubscribe();
    });
  }, [id]);

  useEffect(() => {
    if (teamName && userData && userData.role !== 'author') {
      const unsubscribe = getLiveTeamMembers(teamName, (snapshot) => {
        const members = snapshot.val();

        setTeamMembers(members);
        const currentMember = Object.entries(members).filter(
          (member) => member[0] === userData?.username,
        );

        if (currentMember.length > 0 && currentMember[0][1] === 'owner') {
          setIsOwner(true);
          setTeamOwner(currentMember[0][0]);
        }

        return () => unsubscribe();
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, userData]);

  useEffect(() => {
    if (userData?.role === 'author') {
      setIsAuthor(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData?.username]);

  const handleReviewerStatus = (value) => {
    if (
      book.reviews.firstReviewer.name === userData.username ||
      book.reviews.secondReviewer.name === userData.username
    ) {
      if (book.reviews.firstReviewer.name === userData.username) {
        setCurrentStatus(value);
      } else {
        setCurrentStatus(value);
      }
    }
  };

  const handleSubmitButton = () => {
    if (book.reviews.firstReviewer.name === userData.username) {
      updateFirstReviewerStatus(id, currentStatus);
    } else if (book.reviews.secondReviewer.name === userData.username) {
      updateSecondReviewerStatus(id, currentStatus);
    }

    if (book.reviews)
      if (book.reviews.firstReviewer.name === userData.username) {
        if (currentStatus === '✅ Approve') {
          if (book.reviews.secondReviewer.status === '✅ Approve') {
            updateBookStatus(book.id, 'approved');
          }
        } else if (currentStatus !== '✅ Approve' && commentValue.length === 0) {
          return warningNotification('Please fill all requested fields');
        }
      } else if (book.reviews.secondReviewer.name === userData.username) {
        if (currentStatus === '✅ Approve') {
          if (book.reviews.firstReviewer.status === '✅ Approve') {
            updateBookStatus(book.id, 'approved');
          }
        } else if (currentStatus !== '✅ Approve' && commentValue.length === 0) {
          return warningNotification('Please fill all requested fields');
        }
      }
    if (commentValue.length > 0) {
      sendNotification([book.author], `${commentValue}`);
    }
    if (teamName) {
      navigate(`/my-teams/books/${teamName}`, { replace: true });
    } else {
      navigate(`/my-reviews`, { replace: true });
    }
  };
  return (
    <>
      {userData ? (
        <Card withBorder radius="md" p="md" className={classes.card}>
          <BookBasicInfo book={book} teamName={teamName} bookId={id} userData={userData} />
          {isOwner && book.currentTeam === 1 ? (
            <OwnerView
              isOwner={isOwner}
              book={book}
              teamName={teamName}
              bookId={id}
              teamMembers={teamMembers}
              userData={userData}
            />
          ) : (
            ''
          )}
          {!isAuthor && book.currentTeam === 1 && (
            <CurrentReviewers book={book} bookId={id} teamName={teamName} />
          )}

          <Card.Section className={classes.dropdown} position="center">
            <SimpleGrid
              cols={4}
              spacing="lg"
              breakpoints={[
                { maxWidth: 'md', cols: 3, spacing: 'md' },
                { maxWidth: 'sm', cols: 2, spacing: 'sm' },
                { maxWidth: 'xs', cols: 1, spacing: 'sm' },
                { maxHeight: 120, col: 3 },
              ]}
            >
              <DownloadableLink
                book={book}
                files={book.files}
                title={'Download the whole script here'}
              />
              {isAuthor &&
                (book.reviews?.firstReviewer.status === '⁉️ Request Changes' ||
                  book.reviews?.secondReviewer.status === '⁉️ Request Changes') && (
                  <Card.Section className={classes.sectionScript} position="center">
                    <Text mt="md" color="dimmed" order={4} weight={500}>
                      Upload the revised files here
                    </Text>
                    <UploadPopover
                      book={book}
                      isAuthor={isAuthor}
                      title={'Please upload the updated files'}
                      teamName={teamName}
                    />
                  </Card.Section>
                )}
              {book.reviews?.firstReviewer.name === userData.username ||
              book.reviews?.secondReviewer.name === userData.username ? (
                <Select
                  data={['✅ Approve', '⁉️ Request Changes', ' ❌ Decline']}
                  label="Please select the item status"
                  placeholder="📖 Item Status"
                  clearButtonLabel="Clear selection"
                  clearable={false}
                  required
                  mt={18}
                  onChange={(value) => handleReviewerStatus(value)}
                />
              ) : (
                ''
              )}
            </SimpleGrid>
          </Card.Section>
          {book.reviews?.firstReviewer.name === userData.username ||
          book.reviews?.secondReviewer.name === userData.username ? (
            <AddComment
              handleSubmitButton={handleSubmitButton}
              setCommentValue={setCommentValue}
              commentValue={commentValue}
            />
          ) : (
            ''
          )}
          {!isAuthor &&
          book.currentTeam !== 1 &&
          (book.currentTeam === 2 || userData.teams
            ? userData.teams[teamNames[1]] === true
            : false) ? (
            // <Card.Section className={classes.secondReviewer} position="center">
            <>
              <SecondTeamUser book={book} isAuthor={isAuthor} />

              {/* // </Card.Section> */}
            </>
          ) : (
            ''
          )}
          {book.currentTeam !== 1 && isAuthor ? (
            <SecondTeamAuthorView book={book} isAuthor={isAuthor} />
          ) : (
            ''
          )}
          {(isAuthor || (book.currentTeam !== 1 && book.currentTeam !== 2)) && (
            // <Card.Section className={classes.secondReviewer} position="center">
            <>
              <ThirdTeamUser book={book} isAuthor={isAuthor} teamName={teamName} />

              {/* // </Card.Section> */}
            </>
          )}
          {(isAuthor ||
            (book.currentTeam !== 1 && book.currentTeam !== 2 && book.currentTeam !== 3)) && (
            // <Card.Section className={classes.secondReviewer} position="center">
            <>
              <FourthTeamUser book={book} isAuthor={isAuthor} teamName={teamName} />

              {/* // </Card.Section> */}
            </>
          )}
        </Card>
      ) : (
        <Loader />
      )}
    </>
  );
}
