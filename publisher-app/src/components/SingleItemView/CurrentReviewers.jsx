import { Badge, Button, Card, createStyles, Group, Text, useMantineTheme } from '@mantine/core';
import React, { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router';
import AppContext from '../../providers/AppContext';
import {
  getLiveSingleBook,
  updateFirstReviewerName,
  updateFirstReviewerStatus,
  updateSecondReviewerName,
} from '../../services/book.services';
import { sendNotification } from '../../services/notification.services';
import { addToUserReviewing } from '../../services/user.services';
import Loader from '../SharedComponents/Loader/Loader';
import { singleItemStyles } from './SingleItemViewStyles';
const useStyles = createStyles((theme) => singleItemStyles(theme));
export default function CurrentReviewers({ book, bookId, teamName }) {
  const { classes } = useStyles();
  const { userData } = useContext(AppContext);
  const theme = useMantineTheme();
  const [visibleAssign, setVisibleAssign] = useState(false);
  const handleAssign = () => {
    if (
      !book.reviews.firstReviewer.name &&
      book.reviews.secondReviewer.name !== userData.username
    ) {
      updateFirstReviewerName(bookId, userData.username);
      sendNotification([userData.username], `You have new assignment`);
    } else if (
      !book.reviews.secondReviewer.name &&
      book.reviews.firstReviewer.name !== userData.username
    ) {
      updateSecondReviewerName(bookId, userData.username);
      sendNotification([userData.username], `You have new assignment`);
    }
    addToUserReviewing(userData.username);
  };

  useEffect(() => {
    const unsubscribe = getLiveSingleBook(bookId, (snapshot) => {
      const book = snapshot.val();

      if (
        (book?.reviews?.firstReviewer.name === '' || book?.reviews?.secondReviewer.name === '') &&
        bookId &&
        book?.reviews?.secondReviewer.name !== userData.username &&
        book?.reviews?.firstReviewer.name !== userData.username &&
        book?.teams[teamName] === false
      ) {
        setVisibleAssign(true);
      } else {
        setVisibleAssign(false);
      }
    });
    return () => unsubscribe();
  }, [visibleAssign]);

  return (
    <>
      {userData ? (
        <Card.Section className={classes.sectionReviewers}>
          <Text mt="md" className={classes.label} color="dimmed">
            Reviewers
          </Text>
          {visibleAssign ? (
            <Button onClick={handleAssign} size="12px">
              Assign to myself
            </Button>
          ) : (
            ''
          )}

          <Group spacing={7} mt={5}>
            <Badge color={theme.colorScheme === 'dark' ? 'dark' : 'gray'} key={'reviewer1'}>
              {`${book.reviews?.firstReviewer.name ? book.reviews?.firstReviewer.name : ''} - ${
                book.reviews?.firstReviewer.status
                  ? book.reviews?.firstReviewer.status
                  : '⌛️ Pending'
              }`}
            </Badge>
            <Badge color={theme.colorScheme === 'dark' ? 'dark' : 'gray'} key={'reviewer2'}>
              {`${book.reviews?.secondReviewer.name ? book.reviews?.secondReviewer.name : ''} - ${
                book.reviews?.secondReviewer.status
                  ? book.reviews?.secondReviewer.status
                  : '⌛️ Pending'
              }`}
            </Badge>
          </Group>
        </Card.Section>
      ) : (
        <Loader />
      )}
    </>
  );
}
