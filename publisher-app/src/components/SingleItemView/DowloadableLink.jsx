import { Badge, Group, Text } from '@mantine/core';
import React from 'react';

export default function DownloadableLink({ book, files, title, position }) {
  return (
    <Group position={position}>
      <Text mt="xs" color="dimmed" order={4} weight={500}>
        {title}
      </Text>
      {typeof files === 'object' ? (
        files.length > 0 ? (
          files?.map((file) => (
            <Badge color={'cyan'} key={file} mt={'10px'}>
              <a href={file} key={file} target="_blank" rel="noreferrer">{`${file
                .split('/')[7]
                .split('?')[0]
                .substring(8)
                .replace('%2F', '.')
                .replace('%20', ' ')
                .replace('%20', ' ')
                .replace('%20', ' ')
                .replace('%20', ' ')}`}</a>
            </Badge>
          ))
        ) : (
          <Badge color={'cyan'}>
            <div>No files attached</div>
          </Badge>
        )
      ) : (
        <Badge color={'dark'}>
          <div>No files attached</div>
        </Badge>
      )}
    </Group>
  );
}
