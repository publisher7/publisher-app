import { Button, Group, Popover } from '@mantine/core';
import { useState } from 'react';
import { Upload } from 'tabler-icons-react';

import {
  addAuthorsContractFile,
  addNewContractFile,
  addNewFile,
  updateFourthTeamFile,
  updateFourthTeamFileImage,
  updateSecondTeamBookStatus,
  updateThirdTeamBookStatus,
  updateThirdTeamFile,
} from '../../services/book.services';
import UploadFileDropZone from '../AddResume/UploadFileDropZone';
import { ButtonSecondary } from '../SharedComponents/Buttons';

export function UploadPopover({ book, title = 'Upload your files', teamName }) {
  const [opened, setOpened] = useState(false);
  const [file, setFile] = useState([]);
  const getFileData = (files) => {
    setFile(files);
  };

  const handleSubmitFile = () => {
    if (book.currentTeam === 3) {
      updateThirdTeamFile(book.id, file);
      updateThirdTeamBookStatus(book.id, 'approved', book.currentTeam);
    } else if (book.currentTeam === 4) {
      if (title === 'Add image preview') {
        updateFourthTeamFileImage(book.id, file);
      }
      updateFourthTeamFile(book.id, file);
    } else if (book.currentTeam === 1) {
      addNewFile(book.id, file);
    } else {
      if (book.team2?.status === 'first') {
        addAuthorsContractFile(book.id, file);
        updateSecondTeamBookStatus(book.id, 'second');
      } else if (book.team2?.status === 'second') {
        addNewContractFile(book.id, file);
        updateSecondTeamBookStatus(book.id, 'third');
      } else if (book.team2?.status === 'third') {
        addNewContractFile(book.id, file);
        updateSecondTeamBookStatus(book.id, 'fourth', 'approved');
      }
    }

    setOpened(false);
  };

  return (
    <>
      <Group>
        <Popover
          width={'100%'}
          height={'100%'}
          opened={opened}
          onClose={() => setOpened(false)}
          position="bottom"
          placement="center"
          withCloseButton
          mt={10}
          mb={10}
          title="Upload files "
          // transition="pop-top-right"
          // <ActionIcon variant={theme.colorScheme === 'dark' ? 'hover' : 'light'} onClick={() => setOpened((o) => !o)}>
          //   <Upload size={16} />
          // </ActionIcon>
          target={
            <ButtonSecondary text={title} mt={10} size="xs" leftIcon={<Upload />} onClick={() => setOpened((o) => !o)}>
              {' '}
              {title}
            </ButtonSecondary>
          }
        >
          <UploadFileDropZone onChange={getFileData} files={file} />
          {file.length === 0 ? (
            <Button onClick={handleSubmitFile} loading>
              Submit
            </Button>
          ) : (
            <Button onClick={handleSubmitFile}>Submit</Button>
          )}
        </Popover>
      </Group>
    </>
  );
}
