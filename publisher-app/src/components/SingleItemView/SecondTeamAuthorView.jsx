import { Card, createStyles, Highlight, Text } from '@mantine/core';
import { UploadPopover } from './UploadPopover';
import { singleItemStyles } from './SingleItemViewStyles';
import DownloadableLink from './DowloadableLink';

const useStyles = createStyles((theme) => singleItemStyles(theme));

export default function SecondTeamAuthorView({ book, isAuthor }) {
  const { classes } = useStyles();

  return (
    <>
      {book.currentTeam !== 1 && isAuthor && book.team2.status === 'first' ? (
        <Card.Section className={classes.dropdown} position="center">
          <Text mt="md" color="dimmed" order={4} weight={500}>
            Please upload here a scanned ID card and your IBAN in order to complete your contract
          </Text>
          <UploadPopover book={book} isAuthor={isAuthor}></UploadPopover>
        </Card.Section>
      ) : (
        ''
      )}
      {book.currentTeam !== 1 && isAuthor ? (
        <DownloadableLink files={book.contract?.files} title={'Authors personal information'} />
      ) : (
        ''
      )}
      {book.currentTeam !== 1 && isAuthor && book.team2.status === 'third' ? (
        <Card.Section className={classes.dropdown} position="center">
          <Highlight highlight={'SIGNED'} mt="md" color="dimmed" order={4} weight={500}>
            Please read careful the contract and upload the scanned SIGNED version of contract
          </Highlight>
          <UploadPopover book={book} isAuthor={isAuthor}></UploadPopover>
        </Card.Section>
      ) : (
        ''
      )}
    </>
  );
}
