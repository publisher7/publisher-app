import { Button, Card, createStyles, Text } from '@mantine/core';
import React, { useContext, useEffect, useState } from 'react';
import DownloadableLink from './DowloadableLink';
import { UploadPopover } from './UploadPopover';
import { singleItemStyles } from './SingleItemViewStyles';
import AppContext from '../../providers/AppContext';
import { getLiveSingleBook, updateContractReviewer } from '../../services/book.services';
import { addToUserReviewing } from '../../services/user.services';

const useStyles = createStyles((theme) => singleItemStyles(theme));

export default function SecondTeamUser({ book, isAuthor }) {
  const { classes } = useStyles();
  const [visibleAssign, setVisibleAssign] = useState(false);
  const { userData } = useContext(AppContext);

  const handleAssign = () => {
    updateContractReviewer(book.id, userData.username);
    addToUserReviewing(userData.username);
  };

  useEffect(() => {
    const unsubscribe = getLiveSingleBook(book.id, (snapshot) => {
      const book = snapshot.val();

      if (!book?.contract?.reviewer) {
        setVisibleAssign(true);
      } else {
        setVisibleAssign(false);
      }
    });
    return () => unsubscribe();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [visibleAssign]);

  return (
    <>
      <Card.Section className={classes.sectionScript} position="center">
        <DownloadableLink files={book?.contract?.files} title={'Authors personal information'} position={'left'} />
      </Card.Section>
      {!isAuthor && book.contract?.reviewer === userData.username && book.team2.status === 'second' && (
        <Card.Section className={classes.sectionScript} position="center">
          <Text mt="md" color="dimmed" order={4} weight={500}>
            Upload contract here
          </Text>
          <UploadPopover book={book}></UploadPopover>
        </Card.Section>
      )}
      {visibleAssign ? (
        <Button onClick={handleAssign} size="12px">
          Assign contract to myself
        </Button>
      ) : (
        ''
      )}
      {/* {!isAuthor && book.contract.reviewer === userData.username && book.team2 === 'fourth' && (
        <Card.Section className={classes.sectionScript} position="center">
          <Text mt="md" color="dimmed" order={4} weight={500}>
            Upload contract here
          </Text>
          <Badge></Badge>
        </Card.Section>
      )} */}
      {/* // </Card.Section> */}
    </>
  );
}
