import { Button, Card, createStyles, Group, Image, Text } from '@mantine/core';
import { useContext, useEffect, useState } from 'react';
import AppContext from '../../providers/AppContext';
import {
  getLiveSingleBook,
  updateBookDesign,
  updateFinalStatus,
  updateFourthTeamBookStatus,
  updateFourthTeamReviewer,
} from '../../services/book.services';
import { addToUserReviewing } from '../../services/user.services';
import { ButtonPrimary } from '../SharedComponents/Buttons';
import DownloadableLink from './DowloadableLink';
import { singleItemStyles } from './SingleItemViewStyles';
import { UploadPopover } from './UploadPopover';

const useStyles = createStyles((theme) => singleItemStyles(theme));
export default function FourthTeamUser({ book, isAuthor, teamName }) {
  const { userData } = useContext(AppContext);
  const { classes } = useStyles();
  const [visibleAssign, setVisibleAssign] = useState(false);
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [isApproved, setIsApproved] = useState(false);

  const handleAssign = () => {
    updateFourthTeamReviewer(book.id, userData.username);
    addToUserReviewing(userData.username);
  };
  const handleSubmit = () => {
    updateBookDesign(book.id);
  };
  const handleApproved = () => {
    updateFourthTeamBookStatus(book.id, 'approved', book.currentTeam);
    updateFinalStatus(book.id, 'approved');
    setIsApproved(false);
  };
  useEffect(() => {
    const unsubscribe = getLiveSingleBook(book.id, (snapshot) => {
      const book = snapshot.val();

      if (!book?.team4?.reviewer) {
        setVisibleAssign(true);
      } else {
        setVisibleAssign(false);
      }
      if (!book?.team4?.design) {
        setIsSubmitted(true);
      } else {
        setIsSubmitted(false);
      }
      if (book?.finalStatus !== 'approved') {
        setIsApproved(true);
      } else {
        setIsApproved(false);
      }
    });
    return () => unsubscribe();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [visibleAssign]);
  return (
    <>
      {book.currentTeam !== 1 && book.currentTeam !== 2 && book.currentTeam !== 3 && (
        <Card.Section mt={20} position="center">
          <Card.Section className={classes.sectionScript} position="center">
            <Text mt="xs" color="dimmed" order={4} weight={500}>
              Book Cover Preview
            </Text>
            {book?.team4?.reviewer === userData.username && (
              <>
                <UploadPopover title={'Add image preview'} book={book} isAuthor={isAuthor} />
                <DownloadableLink files={book?.team4?.image} title={'Book Cover Files'} position={'left'} />
              </>
            )}
            {book?.team4?.image && (
              <Card.Section className={classes.imageSection}>
                <Image radius="md" src={book.team4.image} alt="Random unsplash image" width={400} height={450} />
              </Card.Section>
            )}
          </Card.Section>
          <Card.Section className={classes.sectionScript} position="center">
            <DownloadableLink files={book?.team4?.files} title={'Book Design Files'} position={'left'} />
            {book?.team4?.reviewer === userData.username && book.finalStatus !== 'approved' && (
              <UploadPopover title={'Upload Designs File'} book={book} isAuthor={isAuthor} />
            )}
          </Card.Section>
          {visibleAssign && !isAuthor && (
            <Button onClick={handleAssign} size="12px">
              Assign to myself
            </Button>
          )}
          {!isAuthor && isSubmitted && (
            <Group position="right" mt={10}>
              <ButtonPrimary text={'Submit Design'} onClick={handleSubmit} size="30px">
                Submit design
              </ButtonPrimary>
            </Group>
          )}
          {isAuthor && book?.team4?.design && isApproved && (
            <Group position="right" mt={10}>
              <ButtonPrimary text={'Approve Final Design'} onClick={handleApproved} size="30px"></ButtonPrimary>
            </Group>
          )}
        </Card.Section>
      )}
    </>
  );
}
