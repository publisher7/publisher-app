import { Button, Card, createStyles, Group, Textarea, Title } from '@mantine/core';
import { singleItemStyles } from './SingleItemViewStyles';
const useStyles = createStyles((theme) => singleItemStyles(theme));
export default function AddComment({ handleSubmitButton, commentValue, setCommentValue, text }) {
  const { classes } = useStyles();

  return (
    <Card.Section className={classes.sectionScript} position="center">
      <Title mt="md" color="dimmed" order={5}>
        {text}
      </Title>
      <br />
      <Textarea
        value={commentValue}
        onChange={(e) => setCommentValue(e.currentTarget.value)}
        placeholder="Your comment"
        label="Your comment"
        required
        minRows={2}
      />

      <Group position="right">
        <Button position="right" my="md" type="submit" onClick={handleSubmitButton}>
          Submit
        </Button>
      </Group>
    </Card.Section>
  );
}
