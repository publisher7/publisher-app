import React, { useState, useEffect, useContext } from 'react';
import { Group, Badge, UnstyledButton, ThemeIcon, Text } from '@mantine/core';
import AppContext from '../../providers/AppContext';
import {
  getLiveNotifications,
  getNotificationsCount,
  setNotificationsAsRead,
} from '../../services/notification.services';
import { Link } from 'react-router-dom';
import { useMediaQuery } from '@mantine/hooks';

export default function NotificationsLink({ link, showHideSidebar }) {
  const [notificationsCount, setNotificationsCount] = useState(0);
  const { userData } = useContext(AppContext);
  const midScreen = useMediaQuery('(max-width: 800px)');

  useEffect(() => {
    if (userData) {
      const unsubscribe = getLiveNotifications(userData.username, (snapshot) => {
        if (!snapshot.exists()) {
          return;
        }
        getNotificationsCount(userData.username)
          .then(setNotificationsCount)
          .catch((err) => alert(err));

        return () => unsubscribe();
      });
    }
  }, [userData]);

  console.log(notificationsCount);
  return (
    <>
      {!midScreen && (
        <Link to={link.to} onClick={() => showHideSidebar((o) => !o)}>
          <Group
            sx={(theme) => ({
              '&:hover': {
                backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
                cursor: 'pointer',
              },
            })}
            onClick={() => setNotificationsAsRead(userData.username)}
          >
            <div>
              <UnstyledButton
                sx={(theme) => ({
                  display: 'block',
                  width: '100%',
                  padding: theme.spacing.xs,
                  borderRadius: theme.radius.sm,
                  color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,

                  '&:hover': {
                    backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
                  },
                })}
              >
                <Group>
                  <ThemeIcon color={link.color} variant="light">
                    {link.icon}
                  </ThemeIcon>

                  <Text size="sm">{link.label}</Text>
                </Group>
              </UnstyledButton>
            </div>
            {notificationsCount > 0 && (
              <Badge size="md" variant="dot" mt={5}>
                {notificationsCount}
              </Badge>
            )}
          </Group>
        </Link>
      )}
    </>
  );
}
