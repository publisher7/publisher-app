import { timestampToDate } from '../../common/helpers';
import { Group, Text, Divider } from '@mantine/core';
import { Trash } from 'tabler-icons-react';
import { deleteUserNotification } from '../../services/notification.services';

export default function SingleNotification({ notification, username }) {
  return (
    <>
      <Group position="center">
        <Text size="lg">{notification.message}</Text>
        <Text size="lg">{timestampToDate(notification.createdOn).date}</Text>
        <Trash onClick={() => deleteUserNotification(username, notification.id)} />
      </Group>
      <Divider my="md" mx="auto" sx={{ maxWidth: '450px' }} />
    </>
  );
}
