import React, { useState, useEffect, useContext } from 'react';
import { Indicator } from '@mantine/core';
import { BellRinging } from 'tabler-icons-react';
import { Link } from 'react-router-dom';
import AppContext from '../../providers/AppContext';
import {
  getLiveNotifications,
  getNotificationsCount,
  setNotificationsAsRead,
} from '../../services/notification.services';
import { useMediaQuery } from '@mantine/hooks';

export default function NotificationsLinkMobile() {
  const [notificationsCount, setNotificationsCount] = useState(0);
  const { userData } = useContext(AppContext);
  const midScreen = useMediaQuery('(max-width: 800px)');

  useEffect(() => {
    if (userData) {
      const unsubscribe = getLiveNotifications(userData.username, (snapshot) => {
        if (!snapshot.exists()) {
          return;
        }
        getNotificationsCount(userData.username)
          .then(setNotificationsCount)
          .catch((err) => alert(err));

        return () => unsubscribe();
      });
    }
  }, [userData]);

  return (
    <>
      {midScreen && (
        <Link to="notifications" onClick={() => setNotificationsAsRead(userData?.username)}>
          {notificationsCount > 0 ? (
            <Indicator inline offset={4} color="red">
              <BellRinging color="violet" />
            </Indicator>
          ) : (
            <BellRinging color="violet" />
          )}
        </Link>
      )}
    </>
  );
}
