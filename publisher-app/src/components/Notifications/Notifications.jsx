import { useContext, useEffect, useState } from 'react';
import './Notifications.css';
import { Container, Group, Text, Card, Badge, useMantineTheme } from '@mantine/core';
import AppContext from '../../providers/AppContext';
import { Trash } from 'tabler-icons-react';
import {
  deleteAllUserNotifications,
  getLiveNotifications,
  getUserNotifications,
} from '../../services/notification.services';
import Loader from '../SharedComponents/Loader/Loader';
import { timestampToDate } from '../../common/helpers';
import { deleteUserNotification } from '../../services/notification.services';
import CustomModal from '../SharedComponents/CustomModal';
import { ButtonSecondary } from '../SharedComponents/Buttons';

export default function Notifications() {
  const theme = useMantineTheme();
  const [notificationsData, setNotificationsData] = useState(null);
  const [openConfirmModal, setOpenConfirmModal] = useState(false);
  const { userData } = useContext(AppContext);

  const handleDeleteNotifications = () => {
    deleteAllUserNotifications(userData.username);
    setOpenConfirmModal(false);
  };

  useEffect(() => {
    if (userData?.username) {
      const unsubscribe = getLiveNotifications(userData.username, (snapshot) => {
        if (!snapshot.exists()) {
          return setNotificationsData([]);
        }

        Promise.all(getUserNotifications(snapshot))
          .then((result) => setNotificationsData(result.reverse()))
          .catch((err) => alert(err));

        return () => unsubscribe();
      });
    }
  }, [userData]);

  return (
    <>
      {userData && notificationsData ? (
        <Container fluid={true} my="lg" className="notifications">
          {notificationsData.length > 0 ? (
            <>
              <Group
                position="right"
                my="lg"
                className="delete-all"
                onClick={() => setOpenConfirmModal(true)}
              >
                <ButtonSecondary
                  text="Delete-all"
                  type="button"
                  onClick={() => setOpenConfirmModal(true)}
                  icon={<Trash />}
                />
              </Group>
              {notificationsData.map((notification) => (
                <Card
                  withBorder={true}
                  radius="md"
                  style={{
                    backgroundColor:
                      theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0],
                  }}
                  mt="xs"
                  key={notification.id}
                  shadow="sm"
                  p="md"
                >
                  <Group position="right">
                    <>
                      <Badge size="md" variant="outline">
                        {timestampToDate(notification.createdOn).date}
                      </Badge>
                      <Trash
                        size={16}
                        className="delete-icon"
                        onClick={() => deleteUserNotification(userData.username, notification.id)}
                      />
                    </>
                  </Group>
                  <Text weight={500} size="lg">
                    {notification.message}
                  </Text>
                </Card>
              ))}
            </>
          ) : (
            <div>No notifications</div>
          )}
          <CustomModal
            title="Delete all of your notifications?"
            open={openConfirmModal}
            setOpen={setOpenConfirmModal}
            callback={handleDeleteNotifications}
          />
        </Container>
      ) : (
        <Loader />
      )}
    </>
  );
}
