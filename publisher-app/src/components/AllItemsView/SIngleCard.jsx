import { Card, Text, Group, Badge, createStyles, Button, Title, Tooltip, Loader } from '@mantine/core';
import { Book } from 'tabler-icons-react';
import Content from './SeeMore';
import { Link } from 'react-router-dom';
import CurrentReviewers from '../SingleItemView/CurrentReviewers';
import { UserInfoIcons } from '../SingleItemView/testauthor';
import { singleCardStyles } from './SingleCardStyles';
import { ButtonSecondary } from '../SharedComponents/Buttons';
import {
  updateBookStatus,
  updateBookToAnotherTeam,
  updateStatus,
  updateTeam2Status,
  updateThirdTeamFile,
  updateThirdTeamReviewer,
} from '../../services/book.services';
import { messageToAuthor, teamNames } from '../../common/helpers';
import { sendNotification } from '../../services/notification.services';
import { addToUserTotalReviews, removeFromUserReviewing } from '../../services/user.services';
import { useContext } from 'react';
import AppContext from '../../providers/AppContext';

const useStyles = createStyles((theme) => singleCardStyles(theme));

export function SingleCard({ book, teamName, isOwner, activeTab }) {
  const { classes } = useStyles();
  const { userData } = useContext(AppContext);
  const message = messageToAuthor(book.title, book.currentTeam);

  const handleNextTeamClick = () => {
    if (book.currentTeam === 1) {
      updateTeam2Status(book.id);
      removeFromUserReviewing([book.reviews.firstReviewer.name, book.reviews.secondReviewer.name]);
      addToUserTotalReviews([book.reviews.firstReviewer.name, book.reviews.secondReviewer.name]);
    } else if (book.currentTeam === 2) {
      console.log(book.contract.reviewer);
      removeFromUserReviewing([book.contract.reviewer]);
      addToUserTotalReviews([book.contract.reviewer]);
    } else if (book.currentTeam === 3) {
      removeFromUserReviewing([book.team3.reviewer]);
      addToUserTotalReviews([book.team3.reviewer]);
    } else if (book.currentTeam === 4) {
      removeFromUserReviewing([book.team4.reviewer]);
      addToUserTotalReviews([book.team4.reviewer]);
    }
    sendNotification([book.author], message);
    updateBookToAnotherTeam(book.id, teamNames[book.currentTeam], book.author, teamName);
  };

  const handleDelete = () => {
    updateBookStatus(book.id, 'pending');
    if (book.currentTeam === 3) {
      updateThirdTeamReviewer(book.id, '');
      updateThirdTeamFile(book.id, null);
    } else if (book.currentTeam === 2) {
      updateTeam2Status(book.id);
    } else if (book.currentTeam === 1) {
      updateStatus(book.id);
    }
  };

  return (
    <>
      {userData ? (
        <Card withBorder radius="md" className={classes.card}>
          <>
            <Card.Section className={classes.sectionTitle}>
              {teamName && isOwner && activeTab === 4 ? (
                <>
                  <Group position="apart">
                    <Tooltip
                      color={'gray'}
                      wrapLines
                      width={200}
                      withArrow
                      transition="fade"
                      transitionDuration={200}
                      label="Approve sending to the next team"
                    >
                      <Button
                        onClick={(e) => handleNextTeamClick(e)}
                        size={13}
                        variant="gradient"
                        gradient={{ from: '#15aabf', to: '#07daf7b5', deg: 35 }}
                      >
                        {`Send to:`}
                        <br />
                        {`${teamNames[book.currentTeam]}`}
                      </Button>
                    </Tooltip>
                    <Tooltip
                      color={'gray'}
                      wrapLines
                      width={200}
                      withArrow
                      transition="fade"
                      transitionDuration={200}
                      label="Return to the team for another review"
                    >
                      <Button
                        onClick={(e) => handleDelete(e)}
                        size={15}
                        variant="gradient"
                        gradient={{ from: 'red', to: 'red', deg: 35 }}
                      >
                        Return
                      </Button>
                    </Tooltip>
                    <br />
                  </Group>
                </>
              ) : (
                ''
              )}

              <Group position="apart">
                <Text size="sm" color="dimmed" className={classes.label}>
                  Book Title
                </Text>
                <Group mb={4}>
                  <Tooltip
                    position="bottom"
                    placement="center"
                    wrapLines
                    width={120}
                    withArrow
                    transition="fade"
                    transitionDuration={200}
                    label="Open the whole script info."
                  >
                    {teamName || userData.role !== 'author' ? (
                      <Link state={{ book, teamName }} to={`/books/${book.id}`} className={'link'} property={book}>
                        <ButtonSecondary text={'Review'} icon={<Book />} variant="outline" />
                      </Link>
                    ) : (
                      <Link to={`${book.id} `}>
                        {' '}
                        <ButtonSecondary text={'More Info'} icon={<Book />} />
                      </Link>
                    )}
                  </Tooltip>
                </Group>
              </Group>
              <Group position="apart" mt={-5}>
                <Title order={3} weight={500}>
                  {book.title}
                </Title>
              </Group>
              <Group spacing={3} mt={7}>
                {book.genre?.map((el) => (
                  <Badge key={el + book.id}>{el}</Badge>
                ))}
                <br />
              </Group>
            </Card.Section>
          </>
          <Card.Section className={classes.sectionTitle}>
            <UserInfoIcons authorName={book.author} />
          </Card.Section>
          {book.currentTeam === 1 && userData.role !== 'author' ? (
            <Card.Section className={classes.sectionReviewers} mt="-20px" ml={'-30px'}>
              <CurrentReviewers book={book} />
            </Card.Section>
          ) : (
            ''
          )}
          <Card.Section className={classes.sectionReviewers}>
            <Text size="sm" color="dimmed" className={classes.label}>
              Resume
            </Text>

            <Group spacing={8} mb={-8} mt={-15}>
              <Content text={book.resume} />
            </Group>
          </Card.Section>
        </Card>
      ) : (
        <Loader />
      )}
    </>
  );
}
