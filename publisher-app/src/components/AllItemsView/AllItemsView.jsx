/* eslint-disable react-hooks/exhaustive-deps */
import {
  Title,
  Container,
  Card,
  createStyles,
  Group,
  Grid,
  MultiSelect,
  Button,
  Badge,
  Menu,
  ThemeIcon,
} from '@mantine/core';
import { useEffect, useState } from 'react';
import { AdjustmentsAlt, Search } from 'tabler-icons-react';
import { fromBooksDocument, getLiveBooks } from '../../services/book.services';
import Loader from '../SharedComponents/Loader/Loader';
import { SingleCard } from './SIngleCard';

const useStyles = createStyles((theme) => ({
  card: {
    backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
    borderRadius: theme.radius.lg,
    border: `2px solid #fee7e8`,
    width: '97%',
  },
}));

export default function AllItemsView({ teamBooks, teamName, teamOwner, userData, isOwner, singleUser = '', allBooks }) {
  const { classes } = useStyles();
  const [books, setBooks] = useState([]);
  const [searchedBook, setSearchedBook] = useState([]);
  const [filteredBook, setFilteredBook] = useState([]);
  const [teamBooksIds, setTeamBooksIds] = useState([]);
  const [activeTab, setActiveTab] = useState(0);
  const [label, setLabel] = useState('');
  const [currentUserBooks, setCurrentUserBooks] = useState('');

  useEffect(() => {
    const unsubscribe = getLiveBooks((snapshot) => {
      if (!snapshot.val()) {
        setCurrentUserBooks([]);
      }
      const allBooks = fromBooksDocument(snapshot);
      if (singleUser) {
        const currentUserBooks = allBooks?.filter(
          (book) =>
            book.reviews.firstReviewer.name === userData.username ||
            book.reviews.secondReviewer.name === userData.username
        );
        setCurrentUserBooks(currentUserBooks);
      }
      if (!teamBooks) {
        return <div>Sorry</div>;
      }

      const teamBookArr = Object.values(teamBooks);
      setTeamBooksIds(teamBookArr.map((el) => el));

      const result = allBooks.filter(({ id }) => {
        return teamBooksIds.includes(id);
      });
      let conditional;
      if (currentUserBooks.length > 0) {
        conditional = currentUserBooks;
      } else {
        conditional = result;
      }

      if (activeTab === 0) {
        // console.log(conditional.map((book) => console.log(book.teams[teamName])));
        setBooks(conditional.filter((book) => book.reviewedStatus === 'pending' && book.teams[teamName] === false));
        setLabel('oldest books');
        // setPageCount(Math.ceil(books.length / perPage));
      }
      if (activeTab === 1) {
        setLabel('newest Books');
        setBooks(
          conditional
            .filter(
              (book) =>
                (book.reviewedStatus === 'pending' || book.reviewedStatus === '') && book.teams[teamName] === false
            )
            .reverse()
        );
      } else if (activeTab === 2) {
        const filteredBook = conditional.filter(
          (book) =>
            book?.reviews?.firstReviewer?.status !== '✅ Approve' ||
            book?.reviews?.secondReviewer?.status !== '✅ Approve'
        );
        setBooks(filteredBook.filter((book) => book.reviewedStatus === 'pending' && book.teams[teamName] === false));
        setLabel('Pending Books');
      } else if (activeTab === 3) {
        const filteredBook = allBooks.filter((book) => book.teams[teamName] === true);
        setLabel('completed books');
        setBooks(filteredBook);
      } else if (activeTab === 4) {
        const filteredBook = conditional.filter((book) => book.reviewedStatus === 'approved').reverse();
        setLabel('for final Review');
        setBooks(filteredBook);
      }
    });
    return () => unsubscribe();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    label,
    activeTab,
    teamName,
    teamBooks,
    books[0]?.reviews.firstReviewer.name,
    singleUser,
    currentUserBooks?.length,
    teamBooksIds?.length,
    teamBooksIds[0],
  ]);

  const bookArr = books.map((book) => ({
    value: book.title,
    label: book.title,
    group: 'Search by Books',
  }));

  useEffect(() => {
    const filtered = books.filter((book) => book.title === searchedBook[0]);
    setFilteredBook(filtered);
  }, [searchedBook]);

  return (
    <>
      {userData ? (
        <Card className={classes.card}>
          <Container aria-setsize={50} size={2000}>
            {!singleUser ? (
              <Group position={'apart'} mb={10}>
                <Group>
                  <Title ml={4}>Team Books</Title>

                  <Badge variant="gradient" size="lg" gradient={{ from: 'orange', to: 'red' }}>
                    {books.length}
                  </Badge>
                </Group>
                <MultiSelect
                  icon={<Search size={'20px'} />}
                  data={[...bookArr]}
                  placeholder="Search for"
                  searchable
                  nothingFound="Nothing found"
                  onChange={(value) => setSearchedBook(value)}
                  maxSelectedValues={1}
                  style={{ width: '300px' }}
                  radius="lg"
                />
                <Group>
                  <Badge variant="outline" size="lg" color={'pink'}>
                    {label}
                  </Badge>
                  <Menu
                    control={
                      <Button
                        leftIcon={<AdjustmentsAlt />}
                        type="button"
                        variant="outline"
                        radius={'md'}
                        size="xs"
                        color={'cyan'}
                      >
                        FILTER
                      </Button>
                    }
                  >
                    <Menu.Item
                      icon={
                        <ThemeIcon variant="outline" size={14}>
                          {' '}
                          📅
                        </ThemeIcon>
                      }
                      onClick={() => setActiveTab(0)}
                    >
                      Oldest
                    </Menu.Item>
                    <Menu.Item
                      icon={
                        <ThemeIcon variant="outline" size={14}>
                          {' '}
                          🆕
                        </ThemeIcon>
                      }
                      onClick={() => setActiveTab(1)}
                    >
                      Newest
                    </Menu.Item>
                    <Menu.Item
                      icon={
                        <ThemeIcon variant="light" size={14}>
                          {' '}
                          ⏰
                        </ThemeIcon>
                      }
                      onClick={() => setActiveTab(2)}
                    >
                      Pending
                    </Menu.Item>
                    <Menu.Item
                      icon={
                        <ThemeIcon variant="light" size={14}>
                          {' '}
                          ✅
                        </ThemeIcon>
                      }
                      onClick={() => setActiveTab(3)}
                    >
                      Completed
                    </Menu.Item>
                    {isOwner ? (
                      <Menu.Item
                        icon={
                          <ThemeIcon variant="light" size={14}>
                            {' '}
                            👁️‍🗨️
                          </ThemeIcon>
                        }
                        onClick={() => setActiveTab(4)}
                      >
                        For Final Review
                      </Menu.Item>
                    ) : (
                      ''
                    )}
                  </Menu>
                </Group>
              </Group>
            ) : (
              <Group my="lg">
                <Title ml={4}>My books for review</Title>

                <Badge variant="gradient" size="lg" gradient={{ from: 'orange', to: 'red' }}>
                  {currentUserBooks?.length ? currentUserBooks.length : 0}
                </Badge>
              </Group>
            )}
            <Grid
              columns={3}
              spacing="lg"
              breakpoints={[
                { maxWidth: 'md', cols: 2, spacing: 'md' },
                { maxWidth: 'sm', cols: 2, spacing: 'sm' },
                { maxWidth: 'xs', cols: 1, spacing: 'sm' },
                { maxHeight: 120, col: 3 },
              ]}
            >
              {singleUser &&
                currentUserBooks &&
                currentUserBooks.map((book) => (
                  <Grid.Col span={3} md={2} lg={1} key={book.id + book.genre}>
                    <SingleCard book={book} teamName={teamName} isOwner={false} activeTab={activeTab} />
                  </Grid.Col>
                ))}
              {!singleUser && books.length === 0 ? (
                <div className={'noBook'}>No books yet</div>
              ) : filteredBook.length === 0 ? (
                books.map((book) => (
                  <Grid.Col span={3} md={2} lg={1} key={book.id + book.genre}>
                    <SingleCard
                      book={book}
                      teamName={teamName}
                      isOwner={isOwner}
                      activeTab={activeTab}
                      currentUserBooks={currentUserBooks}
                    />
                  </Grid.Col>
                ))
              ) : (
                filteredBook.map((book) => (
                  <Grid.Col span={3} md={2} lg={1} key={book.id + book.genre}>
                    <SingleCard book={book} teamName={teamName} isOwner={isOwner} activeTab={activeTab} />
                  </Grid.Col>
                ))
              )}
            </Grid>
          </Container>
          {/* <Pagination page={activePage} onChange={setPage} total={10} /> */}
        </Card>
      ) : (
        <Loader />
      )}
    </>
  );
}
