// import {useState} from 'react';
// import {TransferList, TransferListData} from '@mantine/core';

// const initialValues = [
// 	[
// 		{value: 'react', label: 'React'},
// 		{value: 'ng', label: 'Angular'},
// 		{value: 'next', label: 'Next.js'},
// 		{value: 'blitz', label: 'Blitz.js'},
// 		{value: 'gatsby', label: 'Gatsby.js'},
// 		{value: 'vue', label: 'Vue'},
// 		{value: 'jq', label: 'jQuery'},
// 	],
// 	[
// 		{value: 'sv', label: 'Svelte'},
// 		{value: 'rw', label: 'Redwood'},
// 		{value: 'np', label: 'NumPy'},
// 		{value: 'dj', label: 'Django'},
// 		{value: 'fl', label: 'Flask'},
// 	],
// ];

// export default function Demo2() {
// 	const [data, setData] = useState(initialValues);
// 	return (
// 		<TransferList
// 			value={data}
// 			onChange={setData}
// 			searchPlaceholder="Search..."
// 			nothingFound="Nothing here"
// 			titles={['Frameworks', 'Libraries']}
// 			breakpoint="sm"
// 		/>
// 	);
// }

import {useState} from 'react';
import {
	Checkbox,
	Group,
	Avatar,
	Text,
	TransferList,
	TransferListData,
	TransferListItemComponent,
	TransferListItemComponentProps,
} from '@mantine/core';

const mockdata = [
	{
		value: 'bender',
		image: 'https://img.icons8.com/clouds/256/000000/futurama-bender.png',
		label: 'Bender Bending Rodríguez',
		description: 'Fascinated with cooking, though has no sense of taste',
	},

	{
		value: 'carol',
		image: 'https://img.icons8.com/clouds/256/000000/futurama-mom.png',
		label: 'Carol Miller',
		description: 'One of the richest people on Earth',
	},
	// ...other items
];

const ItemComponent = ({
	data,
	selected,
}) => (
	<Group noWrap>
		<Avatar src={data.image} radius="xl" size="lg" />
		<div style={{flex: 1}}>
			<Text size="sm" weight={500}>
				{data.label}
			</Text>
			<Text size="xs" color="dimmed" weight={400}>
				{data.description}
			</Text>
		</div>
		<Checkbox checked={selected} onChange={() => { }} tabIndex={-1} sx={{pointerEvents: 'none'}} />
	</Group>
);


function Demo2() {
	const [data, setData] = useState([mockdata, []]);
	return (
		<TransferList
			value={data}
			onChange={setData}
			searchPlaceholder="Search employees..."
			nothingFound="No one here"
			titles={['Employees to hire', 'Employees to fire']}
			listHeight={300}
			breakpoint="sm"
			itemComponent={ItemComponent}
			filter={(query, item) =>
				item.label.toLowerCase().includes(query.toLowerCase().trim()) ||
				item.description.toLowerCase().includes(query.toLowerCase().trim())
			}
		/>
	);
}

export default Demo2