import { useState } from 'react';
import './SeeMore.css';
const ReadMore = ({ children }) => {
  const text = children;
  const [isReadMore, setIsReadMore] = useState(true);
  const toggleReadMore = () => {
    setIsReadMore(!isReadMore);
  };
  if (text?.length < 100)
    return (
      <span className="text" onClick={toggleReadMore}>
        {text}
      </span>
    );
  return (
    <span className="text" onClick={toggleReadMore}>
      {isReadMore ? text.slice(0, 100) : text}
      <span onClick={toggleReadMore} className="read-or-hide">
        {isReadMore ? '...read more' : ' show less'}
      </span>
    </span>
  );
};

const Content = ({ text }) => {
  return (
    <div className="containerSeeMore">
      <p>
        <ReadMore>{text}</ReadMore>
      </p>
    </div>
  );
};

export default Content;
