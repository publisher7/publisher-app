import React, { useEffect, useState, useContext } from 'react';
import { useParams } from 'react-router';
import { getLiveTeams, getTeamByTeamName } from '../../services/team.services';
import Loader from '../SharedComponents/Loader/Loader';
import { Text, Group } from '@mantine/core';
import AllItemsView from '../AllItemsView/AllItemsView';
import {
  fromBooksDocument,
  getLiveBooks,
  getLiveTeamBooks,
  getTeamMembersByTeamName,
} from '../../services/book.services';
import AppContext from '../../providers/AppContext';

export default function TeamBooks({ singleUser = '', team }) {
  const [teamData, setTeamData] = useState(null);
  const { teamName } = useParams();
  const [teamBooks, setTeamBooks] = useState({});
  const [teamMembers, setTeamMembers] = useState([]);
  const { userData } = useContext(AppContext);
  const [teamOwner, setTeamOwner] = useState([]);
  const [isOwner, setIsOwner] = useState(false);
  const [allBooks, setAllBooks] = useState([]);

  useEffect(() => {
    getTeamByTeamName(teamName ? teamName : team).then((snapshot) => {
      setTeamData(() => ({ ...snapshot.val() }));
    });
  }, [teamName, team]);
  useEffect(() => {
    const unsubscribe = getLiveBooks((snapshot) => {
      const allBooks = fromBooksDocument(snapshot);
      setAllBooks(allBooks);
    });
    return () => unsubscribe();
  }, [teamName]);

  useEffect(() => {
    getTeamMembersByTeamName(teamName ? teamName : team).then((members) => {
      if (members.val()) {
        setTeamMembers(members.val());
        const currentMember = Object.entries(members?.val()).filter(
          (member) => member[0] === userData?.username,
        );
        if (currentMember.length > 0 && currentMember[0][1] === 'owner') {
          setIsOwner(true);
          setTeamOwner(currentMember[0][0]);
        }
      }
    });
  }, [teamName, userData, isOwner, team]);

  useEffect(() => {
    const unsubscribe = getLiveTeamBooks(teamName ? teamName : team, (snapshot) => {
      // setTeamBooks(null);
      if (!snapshot.val()) return setTeamBooks({});
      setTeamBooks(snapshot.val());
    });
    return () => unsubscribe();
  }, [teamName, team, teamData, isOwner, Object.keys(teamBooks).length]);

  return (
    <>
      {teamData ? (
        <>
          {Object.keys(teamBooks).length > 0 ? (
            <>
              <Group my="xl" align="center" position="center">
                <Text weight={500} sx={{ fontSize: '2rem' }}>
                  {teamName ? teamName : team}
                </Text>
              </Group>
              <AllItemsView
                teamBooks={teamBooks}
                teamName={teamName ? teamName : team}
                teamOwner={teamOwner}
                userData={userData}
                isOwner={isOwner}
                singleUser={singleUser}
                allBooks={allBooks}
              />
            </>
          ) : (
            <Text size="lg">Currently no books</Text>
          )}
        </>
      ) : (
        <Loader />
      )}
    </>
  );
}
