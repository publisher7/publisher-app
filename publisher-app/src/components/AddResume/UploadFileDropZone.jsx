/* eslint-disable no-unused-vars */
import { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import './UploadFileDropZone.css';
import uploadImg from './assets/cloud-upload-regular-240.png';
import fileDefault from './assets/file-blank-solid-240.png';
import fileCSS from './assets/file-css-solid-240.png';
import filePdf from './assets/file-pdf-solid-240.png';
import filePng from './assets/file-png-solid-240.png';
import { ref as storageRef, uploadBytes, getDownloadURL } from 'firebase/storage';
import { storage } from '../../config/firebase-config';
import { X } from 'tabler-icons-react';

const ImageConfig = {
  default: fileDefault,
  pdf: filePdf,
  png: filePng,
  css: fileCSS,
};

const DropFileInput = (props) => {
  const wrapperRef = useRef(null);
  const [fileList, setFileList] = useState([]);
  const onDragEnter = () => wrapperRef.current.classList.add('dragover');
  const onDragLeave = () => wrapperRef.current.classList.remove('dragover');
  const onDrop = () => wrapperRef.current.classList.remove('dragover');
  const onFileDrop = (e) => {
    const newFile = e.target.files[0];

    if (newFile) {
      const updatedList = [...fileList, newFile];
      setFileList(updatedList);
      props.onFileChange(updatedList);
    }
  };

  const fileRemove = (file) => {
    const updatedList = [...fileList];
    updatedList.splice(fileList.indexOf(file), 1);
    setFileList(updatedList);
    props.onFileChange(updatedList);
  };

  return (
    <>
      <div
        ref={wrapperRef}
        className="drop-file-input"
        onDragEnter={onDragEnter}
        onDragLeave={onDragLeave}
        onDrop={onDrop}
      >
        <div className="drop-file-input__label">
          <img src={uploadImg} alt="" />
          <p>Drag & Drop your files here</p>
        </div>
        <input type="file" value="" onChange={onFileDrop} />
      </div>
      {fileList.length > 0 ? (
        <div className="drop-file-preview">
          <p className="drop-file-preview__title">Ready to upload</p>
          {fileList.map((item, index) => (
            <div key={index} className="drop-file-preview__item">
              <img src={ImageConfig[item.type.split('/')[1]] || ImageConfig['default']} alt="" />
              <div className="drop-file-preview__item__info">
                <p>{item.name}</p>
              </div>

              <X onClick={() => fileRemove(item)} />
            </div>
          ))}
        </div>
      ) : null}
    </>
  );
};

DropFileInput.propTypes = {
  onFileChange: PropTypes.func,
};

export default function UploadFileDropZone({ onChange }) {
  const [_, setError] = useState('');
  const [__, setLoading] = useState(false);
  const [url, setUrl] = useState([]);

  const onFileChange = async (file) => {
    if (!file) return setError(`Please select a file!`);
    setLoading(true);
    await file.map(async (el) => {
      if (!el) return setError(`Please select a file!`);
      setLoading(true);
      const picture = storageRef(storage, `Books/${el.name}`);
      uploadBytes(picture, el).then((snapshot) => {
        return getDownloadURL(snapshot.ref).then((url) => {
          setUrl((prev) => [...prev, url]);
        });
      });
    });
  };

  useEffect(() => {
    onChange(url);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [url]);

  return (
    <div className="upload">
      <h2 order={3} className="Title">
        Upload your file here
      </h2>
      <DropFileInput
        onFileChange={(files) => {
          setUrl([]);
          onFileChange(files);
        }}
      />
    </div>
  );
}
