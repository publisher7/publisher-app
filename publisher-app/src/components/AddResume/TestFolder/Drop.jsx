// // // import { Group, Text, useMantineTheme, MantineTheme } from '@mantine/core';
// // // import { Upload, Photo, X, Icon as TablerIcon } from 'tabler-icons-react';
// // // import { Dropzone, DropzoneStatus, IMAGE_MIME_TYPE } from '@mantine/dropzone';

// // // function getIconColor(status, theme) {
// // //   return status.accepted
// // //     ? theme.colors[theme.primaryColor][theme.colorScheme === 'dark' ? 4 : 6]
// // //     : status.rejected
// // //     ? theme.colors.red[theme.colorScheme === 'dark' ? 4 : 6]
// // //     : theme.colorScheme === 'dark'
// // //     ? theme.colors.dark[0]
// // //     : theme.colors.gray[7];
// // // }

// // // function ImageUploadIcon({ status, ...props }) {
// // //   if (status.accepted) {
// // //     return <Upload {...props} />;
// // //   }

// // //   if (status.rejected) {
// // //     return <X {...props} />;
// // //   }

// // //   return <Photo {...props} />;
// // // }

// // // export const dropzoneChildren = (status, theme) => (
// // //   <Group position="center" spacing="xl" style={{ minHeight: 220, pointerEvents: 'none' }}>
// // //     <ImageUploadIcon status={status} style={{ color: getIconColor(status, theme) }} size={80} />

// // //     <div>
// // //       <Text size="xl" inline>
// // //         Drag images here or click to select files
// // //       </Text>
// // //       <Text size="sm" color="dimmed" inline mt={7}>
// // //         Attach as many files as you like, each file should not exceed 5mb
// // //       </Text>
// // //     </div>
// // //   </Group>
// // // );

// // // export default function Demo() {
// // //   const theme = useMantineTheme();
// // //   return (
// // //     <Dropzone
// // //       onDrop={(files) => console.log('accepted files', files)}
// // //       onReject={(files) => console.log('rejected files', files)}
// // //       maxSize={3 * 1024 ** 2}
// // //       accept={IMAGE_MIME_TYPE}
// // //     >
// // //       {(status) => dropzoneChildren(status, theme)}
// // //     </Dropzone>
// // //   );
// // // }

// // // import React, { useRef } from 'react';
// // // import { Text, Group, Button, createStyles, MantineTheme, useMantineTheme } from '@mantine/core';
// // // import { Dropzone, DropzoneStatus, MIME_TYPES } from '@mantine/dropzone';
// // // import { CloudUpload } from 'tabler-icons-react';

// // // const useStyles = createStyles((theme) => ({
// // //   wrapper: {
// // //     position: 'relative',
// // //     marginBottom: 30,
// // //   },

// // //   dropzone: {
// // //     borderWidth: 1,
// // //     paddingBottom: 50,
// // //   },

// // //   icon: {
// // //     color: theme.colorScheme === 'dark' ? theme.colors.dark[3] : theme.colors.gray[4],
// // //   },

// // //   control: {
// // //     position: 'absolute',
// // //     width: 250,
// // //     left: 'calc(50% - 125px)',
// // //     bottom: -20,
// // //   },
// // // }));

// // // function getActiveColor(status, theme) {
// // //   return status.accepted
// // //     ? theme.colors[theme.primaryColor][6]
// // //     : status.rejected
// // //     ? theme.colors.red[6]
// // //     : theme.colorScheme === 'dark'
// // //     ? theme.colors.dark[0]
// // //     : theme.black;
// // // }

// // // export function DropzoneButton() {
// // //   const theme = useMantineTheme();
// // //   const { classes } = useStyles();
// // //   const openRef = useRef();

// // //   return (
// // //     <div className={classes.wrapper}>
// // //       <Dropzone
// // //         openRef={openRef}
// // //         onReject={(files) => console.log('rejected files', files)}
// // //         onDrop={(files) => console.log('accepted files', files)}
// // //         className={classes.dropzone}
// // //         radius="md"
// // //         accept={[MIME_TYPES.docx]}
// // //         maxSize={3000 * 1024 ** 2}
// // //       >
// // //         {(status) => (
// // //           <div style={{ pointerEvents: 'none' }}>
// // //             <Group position="center">
// // //               <CloudUpload size={50} color={getActiveColor(status, theme)} />
// // //             </Group>
// // //             <Text align="center" weight={700} size="lg" mt="xl" sx={{ color: getActiveColor(status, theme) }}>
// // //               {status.accepted ? 'Drop files here' : status.rejected ? 'Pdf file less than 30mb' : 'Upload resume'}
// // //             </Text>
// // //             <Text align="center" size="sm" mt="xs" color="dimmed">
// // //               Drag&apos;n&apos;drop files here to upload. We can accept only <i>.pdf</i> files that are less than 30mb
// // //               in size.
// // //             </Text>
// // //           </div>
// // //         )}
// // //       </Dropzone>

// // //       <Button className={classes.control} size="md" radius="xl" onClick={() => openRef.current()}>
// // //         Select files
// // //       </Button>
// // //     </div>
// // //   );
// // // }

// // import React, { forwardRef } from 'react';
// // import { useDropzone, FileRejection } from 'react-dropzone';
// // import { DefaultProps, Selectors, MantineNumberSize, LoadingOverlay, Box, useMantineDefaultProps } from '@mantine/core';
// // import { assignRef } from '@mantine/hooks';
// // import useStyles from './Styles';

// // // export type DropzoneStylesNames = Selectors<typeof useStyles>;

// // // export interface DropzoneStatus {
// // //   accepted: boolean;
// // //   rejected: boolean;
// // // }

// // // export interface DropzoneProps extends DefaultProps<DropzoneStylesNames> {
// // //   /** Padding from theme.spacing, or number to set padding in px */
// // //   padding?: MantineNumberSize;

// // //   /** Border radius from theme.radius or number to set border-radius in px */
// // //   radius?: MantineNumberSize;

// // //   /** Render children based on dragging state */
// // //   children(status: DropzoneStatus): React.ReactNode;

// // //   /** Disable files capturing */
// // //   disabled?: boolean;

// // //   /** Called when files are dropped into dropzone */
// // //   onDrop(files: File[]): void;

// // //   /** Called when selected files don't meet file restrictions */
// // //   onReject?(fileRejections: FileRejection[]): void;

// // //   /** Display loading overlay over dropzone */
// // //   loading?: boolean;

// // //   /** File types to accept  */
// // //   accept?: string[];

// // //   /** Get open function as ref */
// // //   openRef?: React.ForwardedRef<() => void | undefined>;

// // //   /** Allow selection of multiple files */
// // //   multiple?: boolean;

// // //   /** Set maximum file size in bytes */
// // //   maxSize?: number;

// // //   /** Name of the form control. Submitted with the form as part of a name/value pair. */
// // //   name?: string;
// // // }

// // const defaultProps = {
// //   padding: 'md',
// //   loading: false,
// //   multiple: true,
// //   maxSize: Infinity,
// //   children: () => null,
// // };

// // export const Dropzonebtn = forwardRef((props, ref) => {
// //   const {
// //     className,
// //     padding,
// //     radius,
// //     disabled,
// //     classNames,
// //     styles,
// //     loading,
// //     multiple,
// //     maxSize,
// //     accept,
// //     children,
// //     onDrop,
// //     onReject,
// //     openRef,
// //     name,
// //     ...others
// //   } = useMantineDefaultProps('Dropzone', defaultProps, props);

// //   const { classes, cx } = useStyles({ radius, padding }, { classNames, styles, name: 'Dropzone' });

// //   const { getRootProps, getInputProps, isDragAccept, isDragReject, open } = useDropzone({
// //     onDropAccepted: onDrop,
// //     onDropRejected: onReject,
// //     disabled: disabled || loading,
// //     accept,
// //     multiple,
// //     maxSize,
// //   });

// //   assignRef(openRef, open);

// //   return (
// //     <Box
// //       {...others}
// //       {...getRootProps({ ref })}
// //       className={cx(
// //         classes.root,
// //         {
// //           [classes.active]: isDragAccept,
// //           [classes.reject]: isDragReject,
// //           [classes.loading]: loading,
// //         },
// //         className
// //       )}
// //     >
// //       <LoadingOverlay visible={loading} radius={radius} />
// //       <input {...getInputProps()} name={name} />
// //       {children({ accepted: isDragAccept, rejected: isDragReject })}
// //     </Box>
// //   );
// // });

// import { Group, Text, useMantineTheme, MantineTheme } from '@mantine/core';
// import { Upload, Photo, X, Icon as TablerIcon } from 'tabler-icons-react';
// import { Dropzone, DropzoneStatus, IMAGE_MIME_TYPE } from '@mantine/dropzone';

// function getIconColor(status: DropzoneStatus, theme: MantineTheme) {
//   return status.accepted
//     ? theme.colors[theme.primaryColor][theme.colorScheme === 'dark' ? 4 : 6]
//     : status.rejected
//     ? theme.colors.red[theme.colorScheme === 'dark' ? 4 : 6]
//     : theme.colorScheme === 'dark'
//     ? theme.colors.dark[0]
//     : theme.colors.gray[7];
// }

// function ImageUploadIcon({ status, ...props }: React.ComponentProps<TablerIcon> & { status: DropzoneStatus }) {
//   if (status.accepted) {
//     return <Upload {...props} />;
//   }

//   if (status.rejected) {
//     return <X {...props} />;
//   }

//   return <Photo {...props} />;
// }

// export const dropzoneChildren = (status: DropzoneStatus, theme: MantineTheme) => (
//   <Group position="center" spacing="xl" style={{ minHeight: 220, pointerEvents: 'none' }}>
//     <ImageUploadIcon status={status} style={{ color: getIconColor(status, theme) }} size={80} />

//     <div>
//       <Text size="xl" inline>
//         Drag images here or click to select files
//       </Text>
//       <Text size="sm" color="dimmed" inline mt={7}>
//         Attach as many files as you like, each file should not exceed 5mb
//       </Text>
//     </div>
//   </Group>
// );

// export default function Test() {
//   const theme = useMantineTheme();
//   return (
//     <Dropzone
//       onDrop={(files) => console.log('accepted files', files)}
//       onReject={(files) => console.log('rejected files', files)}
//       maxSize={3 * 1024 ** 2}
//       accept={IMAGE_MIME_TYPE}
//     >
//       {(status) => dropzoneChildren(status, theme)}
//     </Dropzone>
//   );
// }
