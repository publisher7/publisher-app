import React, {forwardRef} from 'react';
import {useDropzone, FileRejection} from 'react-dropzone';
import {
	DefaultProps,
	Selectors,
	MantineNumberSize,
	LoadingOverlay,
	Box,
	useMantineDefaultProps,
} from '@mantine/core';
import {assignRef} from '@mantine/hooks';
import useStyles from './Styles';





const defaultProps = {
	padding: 'md',
	loading: false,
	multiple: true,
	maxSize: Infinity,
};
export const Dropzone = forwardRef((props, ref) => {
	const {
		className,
		padding,
		radius,
		disabled,
		classNames,
		styles,
		loading,
		multiple,
		maxSize,
		accept,
		children,
		onDrop,
		onReject,
		openRef,
		name,
		...others
	} = useMantineDefaultProps('Dropzone', defaultProps, props);

	const {classes, cx} = useStyles({radius, padding}, {classNames, styles, name: 'Dropzone'});

	const {getRootProps, getInputProps, isDragAccept, isDragReject, open} = useDropzone({
		onDropAccepted: onDrop,
		onDropRejected: onReject,
		disabled: disabled || loading,
		accept,
		multiple,
		maxSize,
	});

	assignRef(openRef, open);

	return (
		<Box
			{...others}
			{...getRootProps({ref})}
			className={cx(
				classes.root,
				{
					[classes.active]: isDragAccept,
					[classes.reject]: isDragReject,
					[classes.loading]: loading,
				},
				className
			)}
		>
			<LoadingOverlay visible={loading} radius={radius} />
			<input {...getInputProps()} name={name} />
			{children({accepted: isDragAccept, rejected: isDragReject})}
		</Box>
	);
});