export const customStyles = (theme) => ({
  card: {
    backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
    maxWidth: '100%',
    margin: '0 auto',
  },

  section: {
    borderBottom: `1px solid ${
      theme.colorScheme === 'dark' ? theme.colors.dark[4] : theme.colors.gray[3]
    }`,
    paddingLeft: theme.spacing.md,
    paddingRight: theme.spacing.md,
    paddingBottom: theme.spacing.md,
    width: '100%',
  },
  mobile: {
    borderBottom: `1px solid ${
      theme.colorScheme === 'dark' ? theme.colors.dark[4] : theme.colors.gray[3]
    }`,
    paddingLeft: theme.spacing.sm,
    paddingRight: theme.spacing.sm,
    paddingBottom: theme.spacing.sm,
    width: '100%',
  },

  input: {
    height: 'auto',
    paddingTop: 18,
  },

  label2: {
    position: 'absolute',
    pointerEvents: 'none',
    fontSize: theme.fontSizes.xs,
    paddingLeft: theme.spacing.sm,
    paddingTop: theme.spacing.sm / 2,
    zIndex: 1,
  },
});

export const initialValues = { title: '', genre: [], files: [], resume: '' };

export const genres = [
  'Horror',
  'Thriller',
  'Romance',
  'Drama',
  'Children',
  'Historical',
  'Fiction',
  'Sci-Fi',
  'Western',
  'Action and Adventure',
  'Classics',
  'Fantasy',
  'Short Stories',
  'Biographic',
];
