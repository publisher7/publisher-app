import { useContext, useState } from 'react';
import {
  Card,
  Group,
  createStyles,
  Container,
  MultiSelect,
  TextInput,
  Textarea,
} from '@mantine/core';
import { customStyles, genres, initialValues } from './CreateResumeStyles';
import { useForm } from '@mantine/hooks';
import { addBook } from '../../../services/book.services';
import UploadFileDropZone from '../UploadFileDropZone';
import { warningNotification, successNotification } from '../../../common/helpers';
import { useNavigate } from 'react-router';
import { ButtonPrimary } from '../../SharedComponents/Buttons';
import AppContext from '../../../providers/AppContext';

const useStyles = createStyles((theme) => customStyles(theme));

export function Resume() {
  // eslint-disable-next-line no-unused-vars
  const [_, setFormValues] = useState('');
  const navigate = useNavigate();
  const { classes } = useStyles();
  const [file, setFile] = useState([]);
  const { userData } = useContext(AppContext);

  const form = useForm({ initialValues: { title: '', genre: [], files: [], resume: '' } });
  const getFileData = (files) => {
    setFile(files);
    form.setFieldValue('files', files);
  };

  const handleSubmit = (values) => {
    if (form.values.genre.length === 0) {
      return warningNotification('Please, choose a genre!');
    } else {
      setFormValues(values);
      addBook(values, userData.username);
      form.setValues(initialValues);
      successNotification('Book added successfully');
      navigate(`/author-books/${userData.username}`, { replace: true });
    }
  };

  return (
    <Card withBorder radius="md" className={classes.card}>
      <Group className={classes.section} position="center" p={0}>
        <Container className={classes.section} fluid={true}>
          <form onSubmit={form.onSubmit((values) => handleSubmit(values))}>
            <TextInput
              required
              label="Title"
              placeholder="Example Title"
              classNames={classes.input}
              onChange={(event) => form.setFieldValue('title', event.target.value)}
            />

            <MultiSelect
              required
              style={{ marginTop: 20, zIndex: 2, maxWidth: '450px' }}
              data={genres}
              placeholder="Pick one"
              label="Genre"
              classNames={classes.label2}
              onChange={(val) => form.setFieldValue('genre', val)}
              creatable
              searchable
            />
            <Textarea
              required
              minRows={5}
              multiple={true}
              label="Resume"
              placeholder="Your short resume here"
              classNames={classes.input}
              onChange={(event) => form.setFieldValue('resume', event.target.value)}
            />
            <UploadFileDropZone onChange={getFileData} files={file} />
            <ButtonPrimary text="Submit" type="submit" />
          </form>
        </Container>
      </Group>
    </Card>
  );
}
