import { useMediaQuery } from '@mantine/hooks';
import { Group, SimpleGrid, Card, Avatar, Text, Divider, useMantineTheme } from '@mantine/core';
import './Creators.css';

export default function Creators() {
  const midScreen = useMediaQuery('(max-width: 800px)');
  const smallScreen = useMediaQuery('(max-width: 600px)');
  const theme = useMantineTheme();

  return (
    <>
      {!midScreen ? (
        <div id="creators" className="cover">
          <div className="book">
            <label htmlFor="page-1" className="book__page book__page--1">
              <img
                src="https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/projectImages%2Fdani%20(2).jpg?alt=media&token=99f8f252-66e8-4ec0-97d8-e46e4fdd9621"
                alt="Daniela Komitova"
              />
            </label>
            <label htmlFor="page-2" className="book__page book__page--4">
              <img
                src="https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/projectImages%2FprofileClr.jpg?alt=media&token=3172e6f2-2823-4dda-9086-3b846c71597e"
                alt="Stefan Dimitrov"
              />
            </label>

            {/* <!-- Resets the page --> */}
            <input type="radio" name="page" id="page-1" />

            {/* <!-- Goes to the second page --> */}
            <input type="radio" name="page" id="page-2" />
            <label className="book__page book__page--2">
              <div className="book__page-front">
                <div className="page__content">
                  <span className="page__content-number-front">1</span>
                  <hr className="horizontal-line"></hr>
                  <h1 className="page__content-book-title">Publisher</h1>
                  <h2 className="page__content-author">Daniela Komitova</h2>
                  <p className="page__content-credits">
                    Email address:
                    <span>danielankomitova@gmail.com</span>
                  </p>
                  <p className="page__content-credits">
                    Special thanks to:
                    <span>Stoyan Peshev</span>
                  </p>
                  <div className="page__content-copyright">
                    <p>Powered by:</p>
                    <p>Telerik Academy</p>
                  </div>
                </div>
              </div>
              <div className="book__page-back">
                <div className="page__content">
                  <span className="page__content-number-back">2</span>
                  <hr className="horizontal-line"></hr>
                  <h1 className="page__content-book-title">Publisher</h1>
                  <h2 className="page__content-author">Stefan Dimitrov</h2>
                  <p className="page__content-credits">
                    Email address:
                    <span> stefandim91@gmail.com</span>
                  </p>
                  <p className="page__content-credits">
                    Special thanks to:
                    <span>Nadezhda Hristova</span>
                  </p>
                  <div className="page__content-copyright">
                    <p>Powered by:</p>
                    <p>Telerik Academy</p>
                  </div>
                </div>
              </div>
            </label>
          </div>
        </div>
      ) : (
        <SimpleGrid cols={smallScreen ? 1 : 2} spacing="xl">
          <Card
            style={{
              backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.red[0],
            }}
            shadow="md"
            p="md"
            radius="md"
            withBorder={true}
            sx={{ width: `${smallScreen ? '90%' : '100%'}`, margin: '0 auto' }}
          >
            <Group direction="column" position="center">
              <Card.Section>
                <Avatar
                  size="xl"
                  src="https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/projectImages%2Fdani%20(2).jpg?alt=media&token=99f8f252-66e8-4ec0-97d8-e46e4fdd9621"
                  alt="Daniela Komitova"
                />
              </Card.Section>

              <Text weight={500} size="xl">
                Daniela Komitova
              </Text>
              <Divider sx={{ width: '70%' }} />
              <Text size="lg">danielankomitova@gmail.com</Text>
              <Text size="sm">Powered By</Text>
              <Text size="lg">Telerik Academy</Text>
            </Group>
          </Card>
          <Card
            style={{
              backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.red[0],
            }}
            shadow="md"
            p="md"
            radius="md"
            withBorder={true}
            sx={{ width: `${smallScreen ? '90%' : '100%'}`, margin: '0 auto' }}
          >
            <Group direction="column" position="center">
              <Card.Section>
                <Avatar
                  size="xl"
                  src="https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/projectImages%2FprofileClr.jpg?alt=media&token=3172e6f2-2823-4dda-9086-3b846c71597e"
                  alt="Stefan Dimitrov"
                />
              </Card.Section>
              <Text weight={500} size="xl">
                Stefan Dimitrov
              </Text>
              <Divider sx={{ width: '70%' }} />
              <Text size="lg">danielankomitova@gmail.com</Text>
              <Text size="sm">Powered By</Text>
              <Text size="lg">Telerik Academy</Text>
            </Group>
          </Card>
        </SimpleGrid>
      )}
    </>
  );
}
