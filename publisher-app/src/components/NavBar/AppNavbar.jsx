import { useContext, useState } from 'react';
import {
  Header,
  Group,
  ActionIcon,
  useMantineColorScheme,
  MediaQuery,
  Burger,
  useMantineTheme,
  Modal,
  Image,
  Text,
  Divider,
} from '@mantine/core';
import { useLocalStorage, useMediaQuery } from '@mantine/hooks';
import { MoonStars, Sun } from 'tabler-icons-react';
import AppContext from '../../providers/AppContext';
import { logout } from '../../services/auth.services';
import { useNavigate } from 'react-router';
import { ButtonPrimary, ButtonSecondary } from '../SharedComponents/Buttons';
import { changeLoggedStatus } from '../../services/user.services';
import NotificationsLinkMobile from '../Notifications/NotificationsLinkMobile';
import { Link } from 'react-router-dom';
import logo from './the publisher.png';

export default function AppNavbar({ setOpened, opened }) {
  const smallScreen = useMediaQuery('(max-width: 600px)');
  const { userData, setContext } = useContext(AppContext);
  const [isLogged, setIsLogged] = useLocalStorage({ key: 'isLogged', defaultValue: false });
  const [openConfirmLogOut, setOpenConfirmLogoOut] = useState(false);
  const theme = useMantineTheme();
  const { colorScheme, toggleColorScheme } = useMantineColorScheme();
  const navigate = useNavigate();

  const handleLogOut = () => {
    return logout()
      .then(() => {
        changeLoggedStatus(userData.username, false);
        setIsLogged(false);
        setContext({
          user: null,
          userData: null,
        });
        navigate('/');
      })
      .then(() => setOpenConfirmLogoOut(false))
      .catch((err) => console.log(err));
  };

  return (
    <>
      <Header
        height={70}
        p="md"
        style={{
          backgroundColor:
            theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.red[1],
        }}
      >
        <Group position="apart">
          <Group sx={{ height: '100%' }} spacing="xs" position="left">
            <MediaQuery largerThan="sm" styles={{ display: 'none' }}>
              <Burger
                opened={opened}
                onClick={() => setOpened((o) => !o)}
                size="sm"
                color={theme.colors.gray[6]}
                mr="xl"
              />
            </MediaQuery>
            <ActionIcon variant="default" onClick={() => toggleColorScheme()} size={30}>
              {colorScheme === 'dark' ? <Sun size={16} /> : <MoonStars size={16} />}
            </ActionIcon>
            <Link to="/" onClick={() => setOpened(false)}>
              {smallScreen ? (
                <>
                  <Text ml="sm">Home</Text>
                  <Divider ml="sm" color="dark" size="xs" />
                </>
              ) : (
                <Image src={logo} width={'150px'} height={40} mt={-10}>
                  The Publisher
                </Image>
              )}
            </Link>
          </Group>
          <Group>
            {isLogged && <NotificationsLinkMobile />}
            {isLogged && (
              <ButtonPrimary text="Log Out" onClick={() => setOpenConfirmLogoOut(true)} />
            )}
          </Group>
        </Group>
      </Header>
      <Modal
        size="sx"
        centered
        opened={openConfirmLogOut}
        onClose={() => setOpenConfirmLogoOut(false)}
        title="Are you sure you want to quit?"
      >
        <Group position="center" mt="md" spacing="xl">
          <ButtonPrimary text="Confirm" onClick={handleLogOut} />
          <ButtonSecondary text="Cancel" onClick={() => setOpenConfirmLogoOut(false)} />
        </Group>
      </Modal>
    </>
  );
}
