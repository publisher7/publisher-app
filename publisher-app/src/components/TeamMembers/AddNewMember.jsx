import { forwardRef, useState, useEffect } from 'react';
import { Container, Group, Text, Modal, MultiSelect, Avatar } from '@mantine/core';
import { ButtonPrimary, ButtonSecondary } from '../SharedComponents/Buttons';
import { fromUsersDocument, getAllUsers } from '../../services/user.services';
import { addNewMembers } from '../../services/team.services';
import { successNotification } from '../../common/helpers';

export default function AddNewMember({ showHide, setShowHide, currentMembersIds, teamName }) {
  const [usersData, setUsersData] = useState([]);
  const [chooseUsers, setChooseUsers] = useState([]);

  useEffect(() => {
    getAllUsers().then((snapshot) => {
      const users = fromUsersDocument(snapshot);
      setUsersData(
        users
          .filter((user) => {
            return (
              !currentMembersIds.includes(user.id) &&
              user.role !== 'admin' &&
              user.role !== 'author'
            );
          })
          .map((user) => ({
            value: user.username,
            label: `${user.firstName} ${user.lastName}`,
            image: user.avatarUrl,
            description: user.email,
          }))
      );
    });
  }, [currentMembersIds]);

  const addMembers = (e) => {
    e.preventDefault();
    return addNewMembers(chooseUsers, teamName).then(() => {
      setChooseUsers([]);
      setShowHide(false);
      successNotification('Members added successfully!');
    });
  };

  const userInfoItem = forwardRef(({ image, label, description, ...others }, ref) => (
    <div ref={ref} {...others}>
      <Group noWrap>
        <Avatar src={image} />

        <div>
          <Text>{label}</Text>
          <Text size="xs" color="dimmed">
            {description}
          </Text>
        </div>
      </Group>
    </div>
  ));

  return (
    <Modal
      size="md"
      centered
      opened={showHide}
      onClose={() => setShowHide(false)}
      title="Add new members to your team."
    >
      <Container size="lg">
        <MultiSelect
          value={chooseUsers}
          onChange={setChooseUsers}
          data={usersData}
          itemComponent={userInfoItem}
          label="Select members."
          placeholder="Pick all that you like"
          nothingFound="No users to add"
          maxDropdownHeight={200}
          required
          clearable
          searchable
        />
      </Container>{' '}
      <Group position="center" mt="md" spacing="xl">
        <ButtonPrimary text="Confirm" onClick={addMembers} />
        <ButtonSecondary text="Cancel" onClick={() => setShowHide(false)} />
      </Group>
    </Modal>
  );
}
