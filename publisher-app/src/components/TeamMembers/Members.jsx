import React, { useEffect, useState } from 'react';
import { fromUsersDocument, getLiveUsers } from '../../services/user.services';
import MembersSearch from './MembersSearch';
import SingleMember from './SingleMember';
import AddNewMember from './AddNewMember';
import { removeMember } from '../../services/team.services';
import CustomModal from '../SharedComponents/CustomModal';
import Loader from '../SharedComponents/Loader/Loader';

export default function Members({ members, teamName, userData }) {
  const [teamMembers, setTeamMembers] = useState(null);
  const [searchSubstr, setSearchSubstr] = useState('');
  const [showHideAddMember, setShowHideAddMember] = useState(false);
  const [userToRemove, setUserToRemove] = useState([]);
  const [openConfirmModal, setOpenConfirmModal] = useState(false);

  useEffect(() => {
    if (members) {
      const membersNames = Object.keys(members);
      const unsubscribe = getLiveUsers((snapshot) => {
        setTeamMembers(
          fromUsersDocument(snapshot)
            .filter((member) => membersNames.includes(member.username))
            .map((member) => ({ ...member, teamRole: members[`${member.username}`] })),
        );
      });

      return () => unsubscribe();
    }
  }, [members]);

  const handleRemoveMember = () => {
    const [username, teamName] = userToRemove;
    removeMember(username, teamName);
    setUserToRemove([]);
    setOpenConfirmModal(false);
  };

  return (
    <>
      {teamMembers && members && userData ? (
        <>
          <MembersSearch
            setShowHide={setShowHideAddMember}
            membersCount={Object.keys(members).length}
            setSearchSubstr={setSearchSubstr}
            members={members}
            username={userData.username}
            role={userData.role}
          />
          {teamMembers.map((member) => (
            <SingleMember
              members={members}
              {...member}
              teamName={teamName}
              key={member.username}
              searchSubstr={searchSubstr}
              setUserToRemove={setUserToRemove}
              setOpenConfirmModal={setOpenConfirmModal}
              role={userData.role}
              userData={userData}
            />
          ))}
          <AddNewMember
            showHide={showHideAddMember}
            teamName={teamName}
            setShowHide={setShowHideAddMember}
            currentMembersIds={teamMembers.map((user) => user.id)}
          />
          <CustomModal
            open={openConfirmModal}
            setOpen={setOpenConfirmModal}
            title="Remove current member?"
            callback={handleRemoveMember}
          />
        </>
      ) : (
        <Loader />
      )}
    </>
  );
}
