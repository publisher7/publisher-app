import React, { useEffect, useContext, useState } from 'react';
import { useParams } from 'react-router';
import { getLiveSingleTeam } from '../../services/team.services';
import Loader from '../SharedComponents/Loader/Loader';
import Members from './Members';
import { Text, Group } from '@mantine/core';
import AppContext from '../../providers/AppContext';

export default function TeamMembers() {
  const { userData } = useContext(AppContext);
  const [teamData, setTeamData] = useState(null);
  const { teamName } = useParams();

  useEffect(() => {
    const unsubscribe = getLiveSingleTeam(teamName, (snapshot) => {
      setTeamData(snapshot.val());
    });

    return () => unsubscribe();
  }, [teamName]);

  return (
    <>
      {userData && teamData ? (
        <>
          <Group my="xl" align="center" position="center">
            <Text weight={500} sx={{ fontSize: '2rem' }}>
              {teamName}
            </Text>
          </Group>
          <Members members={teamData.members} teamName={teamName} userData={userData} />
        </>
      ) : (
        <Loader />
      )}
    </>
  );
}
