import { Container, Text, Divider, Grid, TextInput, Badge, Group } from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import { ButtonSecondary } from '../SharedComponents/Buttons';

export default function MembersSearch({ setShowHide, setSearchSubstr, members, username, role }) {
  const midscreen = useMediaQuery('(max-width: 1000px)');

  return (
    <Container fluid={true}>
      <Group position="apart">
        <>
          <Text my="lg" size="xl" weight={500}>
            Members{' '}
            <Badge variant="gradient" size="lg" gradient={{ from: 'orange', to: 'red' }}>
              {Object.keys(members).length}
            </Badge>
          </Text>
        </>
        {(role === 'admin' || members[`${username}`] === 'owner') && (
          <ButtonSecondary text="Add new member" onClick={() => setShowHide(true)} />
        )}
      </Group>
      <Container
        fluid={true}
        sx={(theme) => ({
          backgroundColor:
            theme.colorScheme === 'dark' ? theme.colors.dark[5] : theme.colors.gray[1],
          textAlign: 'center',
          borderRadius: theme.radius.md,
        })}
      >
        <TextInput
          p="md"
          placeholder="Search..."
          onChange={(e) => setSearchSubstr(e.currentTarget.value)}
        />
      </Container>
      {!midscreen && (
        <>
          <Grid columns={13} align="center" my="md" grow={true}>
            <Grid.Col span={4} align="center">
              <Text weight={700}>Account</Text>
            </Grid.Col>
            <Grid.Col span={3} align="center">
              <Text weight={700}>Contacts</Text>
            </Grid.Col>
            <Grid.Col span={2}>
              <Text weight={700} align="center">
                Last seen
              </Text>
            </Grid.Col>
            <Grid.Col span={2} align="center">
              <Text weight={700}>Currently reviewing</Text>
            </Grid.Col>
            <Grid.Col span={2} align="center">
              {' '}
              <Text weight={700}>Total Reviewed</Text>
            </Grid.Col>
          </Grid>
          <Divider size="md" />
        </>
      )}
    </Container>
  );
}
