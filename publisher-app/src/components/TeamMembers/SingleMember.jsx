import './SingleMember.css';
import { Container, Text, Group, Avatar, Divider, Grid, Indicator, Card } from '@mantine/core';
import { Link } from 'react-router-dom';
import { timestampToDate } from '../../common/helpers';
import { UserMinus } from 'tabler-icons-react';
import { useMediaQuery } from '@mantine/hooks';

export default function SingleMember({
  username,
  avatarUrl,
  firstName,
  lastName,
  email,
  isLogged,
  lastOnline,
  searchSubstr,
  currentlyReviewing,
  totalReviewed,
  teamName,
  setUserToRemove,
  setOpenConfirmModal,
  phoneNumber,
  role,
  members,
  userData,
}) {
  const midscreen = useMediaQuery('(max-width: 1000px)');
  const determineDisplay = (str) => {
    return firstName.toLowerCase().includes(str.toLowerCase()) ||
      lastName.toLowerCase().includes(str.toLowerCase()) ||
      email.toLowerCase().includes(str.toLowerCase())
      ? 'block'
      : 'none';
  };

  const handleRemoveClick = () => {
    setUserToRemove([username, teamName]);
    setOpenConfirmModal(true);
  };

  return (
    <Container
      className="team-member"
      key={username}
      fluid={true}
      sx={{ display: `${determineDisplay(searchSubstr)}` }}
      px={midscreen ? 0 : 'md'}
    >
      {!midscreen ? (
        <>
          <Grid columns={13} my="sm" align="center" grow={true}>
            <Grid.Col span={4}>
              <Group>
                <Indicator
                  inline
                  size={16}
                  offset={7}
                  position="bottom-end"
                  color={isLogged ? 'green' : 'red'}
                  withBorder
                  zIndex={20}
                >
                  <Avatar src={avatarUrl} alt={username} size="lg" radius="xl" />
                </Indicator>
                <Link to={`../../profile/${username}`}>
                  <Text>{`${firstName} ${lastName}`}</Text>
                </Link>
                {(role === 'admin' ||
                  (members[`${userData.username}`] === 'owner' &&
                    username !== userData.username)) && (
                  <UserMinus className="remove-member" onClick={handleRemoveClick} />
                )}
              </Group>
            </Grid.Col>
            <Grid.Col span={3}>
              <Group direction="column">
                <Text>{`${email}`}</Text>
                {(userData.role === 'admin' || members[`${userData.username}`] === 'owner') && (
                  <Text>{`${phoneNumber}`}</Text>
                )}
              </Group>
            </Grid.Col>
            <Grid.Col span={2} align="center">
              {isLogged ? (
                <Text weight={500}>online</Text>
              ) : (
                <>
                  {`${timestampToDate(lastOnline).time}`} <br />
                  <Text weight={500}>{`${timestampToDate(lastOnline).date}`}</Text>
                </>
              )}
            </Grid.Col>
            <Grid.Col span={2} align="center">
              <Text>{currentlyReviewing}</Text>
            </Grid.Col>
            <Grid.Col span={2} align="center">
              <Text>{totalReviewed}</Text>
            </Grid.Col>
          </Grid>
          <Divider />
        </>
      ) : (
        <Container fluid={true} p={0}>
          <Card shadow="sm" withBorder={true} my="md">
            <Group position="apart">
              <Text weight={500}>Account</Text>
              <Link to={`../../profile/${username}`}>
                <Text>{`${firstName} ${lastName}`}</Text>
              </Link>
              <Avatar src={avatarUrl} alt={username} size="md" radius="xl" />
            </Group>
            <Divider size="xs" my="sm" />
            <Group position="apart">
              <Text weight={500}>Contacts</Text>
              <Text>{`${email}`}</Text>
              {(userData.role === 'admin' || members[`${userData.username}`] === 'owner') && (
                <Text>{`${phoneNumber}`}</Text>
              )}
            </Group>
            <Divider size="xs" my="sm" />
            <Group position="apart">
              <Text weight={500}>Last Seen</Text>
              {isLogged ? (
                <Text weight={500}>online</Text>
              ) : (
                <>
                  {`${timestampToDate(lastOnline).time}`} <br />
                  <Text weight={500}>{`${timestampToDate(lastOnline).date}`}</Text>
                </>
              )}
            </Group>
            <Divider size="xs" my="sm" />
            <Group position="apart">
              <Text weight={500}>Currently reviewing</Text>
              <Text weight={500}>{currentlyReviewing}</Text>
            </Group>
            <Divider size="xs" my="sm" />
            <Group position="apart">
              <Text weight={500}>Total reviewed</Text>
              <Text weight={500}>{totalReviewed}</Text>
            </Group>
            <Divider size="xs" my="sm" />
          </Card>
        </Container>
      )}
    </Container>
  );
}
