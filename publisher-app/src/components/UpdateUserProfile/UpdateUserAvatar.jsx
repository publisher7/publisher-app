import { useContext, useState } from 'react';
import { Group, Avatar, LoadingOverlay, Text, Grid } from '@mantine/core';
import AppContext from '../../providers/AppContext';
import { ref as storageRef, uploadBytes, getDownloadURL } from 'firebase/storage';
import { storage } from '../../config/firebase-config';
import {
  getUserData,
  reverseDefaultAvatar,
  updateUserProfilePicture,
} from '../../services/user.services';
import { ButtonSecondary, ButtonDanger } from '../SharedComponents/Buttons';
import CustomModal from '../SharedComponents/CustomModal';

export default function UpdateUserAvatar({ smallScreen }) {
  const { user, userData, setContext } = useContext(AppContext);
  const [avatar, setAvatar] = useState(null);
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const [openConfirmModal, setOpenConfirmModal] = useState(false);

  const uploadPicture = (e) => {
    e.preventDefault();

    const file = e.target[0]?.files?.[0];

    if (!file) return setError(`Please select a file!`);
    setLoading(true);
    const picture = storageRef(storage, `usersProfileImages/${userData.username}/avatar`);
    uploadBytes(picture, file)
      .then((snapshot) => {
        return getDownloadURL(snapshot.ref).then((url) => {
          return updateUserProfilePicture(userData.username, url).then(() => {
            setContext({
              user,
              userData: {
                ...userData,
                avatarUrl: url,
              },
            });
            setLoading(false);
          });
        });
      })
      .catch((error) => setError(`${error.message}`));
  };

  const removeAvatar = () => {
    reverseDefaultAvatar(userData.username)
      .then(() =>
        getUserData(user.uid).then((snapshot) => {
          if (!snapshot.exists()) {
            setError('Something went wrong!');
          }

          setContext({
            user,
            userData: snapshot.val()[Object.keys(snapshot.val())[0]],
          });
          setAvatar('');
          setOpenConfirmModal(false);
        }),
      )
      .catch((err) => alert(err.message));
  };

  return (
    <>
      <Grid columns={smallScreen ? 6 : 12}>
        <Grid.Col span={6}>
          <Text size="xl" weight={500}>
            Avatar
          </Text>
          <Text size="lg">
            You can change your avatar here or remove the current and revert to the default.
          </Text>
        </Grid.Col>
        <Grid.Col span={6}>
          <Group position="center">
            <div style={{ width: 120, height: 120, position: 'relative' }}>
              <LoadingOverlay visible={loading} />
              <Avatar
                src={avatar ? avatar : userData.avatarUrl}
                radius="md"
                sx={{ width: '120px', height: '120px' }}
              />
            </div>
            <Group direction="column">
              <form onSubmit={uploadPicture}>
                <Group direction="column">
                  <input
                    type="file"
                    name="file"
                    onChange={(e) => setAvatar(URL.createObjectURL(e.target.files[0]))}
                    id="file"
                    className="inputfile"
                  ></input>
                  <label htmlFor="file">Choose file</label>
                  <ButtonSecondary text="Submit picture" type="submit" />
                </Group>
              </form>
              <ButtonDanger text="Remove avatar" onClick={() => setOpenConfirmModal(true)} />
            </Group>
          </Group>
        </Grid.Col>
      </Grid>
      <CustomModal
        open={openConfirmModal}
        setOpen={setOpenConfirmModal}
        title="Are you sure you want to remove your current avatar?"
        callback={removeAvatar}
      />
    </>
  );
}
