import { Group, Text, Grid, PasswordInput, Box } from '@mantine/core';
import { useLocalStorage } from '@mantine/hooks';
import { updateUserPassword } from '../../services/auth.services';
import { useContext, useState } from 'react';
import { useNavigate } from 'react-router';
import AppContext from '../../providers/AppContext';
import { ButtonSecondary } from '../SharedComponents/Buttons';
import AlertDanger from '../SharedComponents/AlertDanger';

export default function UpdateUserPassword({ smallScreen }) {
  const { user, setContext } = useContext(AppContext);
  const [_, setIsLogged] = useLocalStorage({ key: 'isLogged', defaultValue: false });
  const [newPassword, setNewPassword] = useState({
    password: '',
    confirmPassword: '',
  });
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const updatePassword = (e) => {
    e.preventDefault();
    const { password, confirmPassword } = newPassword;
    if (password !== confirmPassword) {
      return setError(() => 'Passwords do not match!');
    }

    if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/.test(password)) {
      return setError(
        () =>
          'Password must contain at least one number, one lower, one upper case and one special character',
      );
    }

    updateUserPassword(user, password)
      .then(() => {
        setError('');
        setNewPassword({
          password: '',
          confirmPassword: '',
        });
        setIsLogged(false);
        setContext({
          user: null,
          userData: null,
        });

        setTimeout(() => {
          navigate('/');
        }, 1000);
      })
      .catch((err) => setError(() => err.message));
  };

  return (
    <Grid columns={smallScreen ? 6 : 12}>
      <Grid.Col span={6}>
        <Text size="xl" weight={500}>
          Password
        </Text>
        <Text size="lg">
          After a successful password update, you will be redirected to the home page where you can
          log in with your new password.
        </Text>
      </Grid.Col>
      <Grid.Col span={6}>
        <Group position="center" align="center">
          <form onSubmit={updatePassword} autoComplete="off">
            <Group position="center" direction="column">
              <Box sx={{ width: 200 }}>
                <PasswordInput
                  required
                  label="New password"
                  value={newPassword.password}
                  onChange={(e) => setNewPassword({ ...newPassword, password: e.target.value })}
                />
                <PasswordInput
                  required
                  label="Confirm new password"
                  value={newPassword.confirmPassword}
                  onChange={(e) =>
                    setNewPassword({ ...newPassword, confirmPassword: e.target.value })
                  }
                />
              </Box>
              <ButtonSecondary text="Confirm" type="submit" />
            </Group>
          </form>
          {error && <AlertDanger message={error} size={200} />}
        </Group>
      </Grid.Col>
    </Grid>
  );
}
