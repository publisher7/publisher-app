import { Group, TextInput, Text, Grid } from '@mantine/core';
import { useContext, useState } from 'react';
import AppContext from '../../providers/AppContext';
import { updateDBUserEmail } from '../../services/user.services';
import { updateUserEmail } from '../../services/auth.services';
import { ButtonSecondary } from '../SharedComponents/Buttons';
import AlertDanger from '../SharedComponents/AlertDanger';
import { successNotification } from '../../common/helpers';

export default function UpdateUserEmail({ smallScreen }) {
  const { user, userData } = useContext(AppContext);
  const [email, setEmail] = useState('');
  const [error, setError] = useState('');

  const updateEmail = (e) => {
    e.preventDefault();

    if (!/^\S+@\S+$/.test(email)) {
      setError(() => 'Please, enter a valid email address!');
      return;
    }
    updateUserEmail(user, email)
      .then(() => {
        updateDBUserEmail(userData.username, email).then(() => {
          setError('');
          setEmail('');
          setTimeout(() => {
            successNotification('Email updated successfully!');
          }, 500);
        });
      })
      .catch((err) => setError(() => err.message));
  };
  return (
    <Grid columns={smallScreen ? 6 : 12}>
      <Grid.Col span={6}>
        <Text size="xl" weight={500}>
          Change email
        </Text>
        <Text size="lg">Change your email here and upon successful update, you will be notified instantly.</Text>
      </Grid.Col>
      <Grid.Col span={6}>
        <Group position="center">
          <form onSubmit={updateEmail}>
            <Group position="center" direction="column">
              <TextInput
                placeholder="example@mail.com"
                value={email}
                label="Email"
                required
                onChange={(e) => setEmail(e.target.value)}
              />
              <ButtonSecondary text="Confirm" type="submit" />
            </Group>
          </form>
          {error && <AlertDanger message={error} />}
        </Group>
      </Grid.Col>
    </Grid>
  );
}
