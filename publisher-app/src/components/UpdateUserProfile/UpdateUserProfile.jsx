import { Container, Divider, createStyles } from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import { useContext } from 'react';
import AppContext from '../../providers/AppContext';
import Loader from '../SharedComponents/Loader/Loader';
import UpdateUserAvatar from './UpdateUserAvatar';
import UpdateUserEmail from './UpdateUserEmail';
import UpdateUserPassword from './UpdateUserPassword';

const useStyles = createStyles((theme) => ({
  container: {
    backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : null,
  },
}));

export default function UpdateUserProfile() {
  const { userData } = useContext(AppContext);
  const smallScreen = useMediaQuery('(max-width: 650px)');
  const { classes } = useStyles();
  return (
    <>
      {userData ? (
        <Container className={classes.container} mt="md" py="md" px="lg">
          <UpdateUserAvatar smallScreen={smallScreen} />
          <Divider my="lg" />
          <UpdateUserEmail smallScreen={smallScreen} />
          <Divider my="lg" />
          <UpdateUserPassword smallScreen={smallScreen} />
          <Divider my="lg" />
        </Container>
      ) : (
        <Loader />
      )}
    </>
  );
}
