import { ButtonSecondary, ButtonDanger } from '../SharedComponents/Buttons';
import { Group, Modal } from '@mantine/core';

export default function CustomModal({ title, open, setOpen, callback, size = 'sx' }) {
  return (
    <Modal size={size} centered opened={open} onClose={() => setOpen(false)} title={title}>
      {' '}
      <Group position="center" mt="md" spacing="xl">
        <ButtonDanger text="Confirm" onClick={callback} />
        <ButtonSecondary text="Cancel" onClick={() => setOpen(false)} />
      </Group>
    </Modal>
  );
}
