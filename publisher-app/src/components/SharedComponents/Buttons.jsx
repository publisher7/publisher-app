import { Button } from '@mantine/core';

export function ButtonPrimary({ text, type = null, onClick = null, icon = null }) {
  return (
    <Button
      leftIcon={icon ? icon : null}
      variant="gradient"
      gradient={{ from: '#ed6ea0', to: '#ec8c69', deg: 35 }}
      onClick={onClick ? onClick : null}
      type={type ? type : 'button'}
    >
      {text}
    </Button>
  );
}

export function ButtonSecondary({ text, type = null, onClick = null, icon = null, variant = 'outline' }) {
  return (
    <Button
      color="cyan"
      variant={variant}
      leftIcon={icon ? icon : null}
      onClick={onClick ? onClick : null}
      type={type ? type : 'button'}
    >
      {text}
    </Button>
  );
}

export function ButtonDanger({ text, type = null, onClick = null, icon = null }) {
  return (
    <Button
      color="red"
      variant="light"
      leftIcon={icon ? icon : null}
      onClick={onClick ? onClick : null}
      type={type ? type : 'button'}
      sx={{ border: '.5px solid red' }}
    >
      {text}
    </Button>
  );
}
