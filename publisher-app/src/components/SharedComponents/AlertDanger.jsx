import { Alert, Container, Text } from '@mantine/core';

export default function AlertDanger({ message, size = 250 }) {
  return (
    <Container size={250}>
      <Alert color="red" mt="sm" radius="sm" sx={{ border: '.5px solid red' }}>
        <Text size="md" weight={500} align="center" color="red">
          {message}
        </Text>
      </Alert>
    </Container>
  );
}
