import { createContext } from 'react';

const AppContext = createContext({
  user: null,
  userData: null,
  setContext() {
    //  updates the state
    // real implementation comes from App.jsx
  },
});
export default AppContext;
