import {showNotification} from '@mantine/notifications';
import {Check, X} from 'tabler-icons-react';

export const teamNames = [
	'Script Review Department',
	'Proposal and Contract Department',
	'Editing and Proof-Reading Team',
	'Design & Printing',
	'Marketing & Distribution',
];

/**
 * Returns the format of the message to the author based on the input team and title.
 * @param {string} title The title of the book.
 * @param {number} currTeam The current team of the book {book.currentTeam}.
 * @returns {string} The message to send.
 */
export const messageToAuthor = (title, currTeam) => {
	if (currTeam === 1) {
		return `We are happy to inform you that your book: ${title} is approved. Now we are looking forward to receive your personal documents in order to complete your contract offer`;
	}

	if (currTeam === 2) {
		return `We are happy to inform you that we have received your signed contract regarding ${title}. You will be notified when the final draft for your book is ready.`;
	}
	if (currTeam === 3) {
		return `We are happy to inform you that the final draft for your ${title} is now ready. We are looking forward for your approval of your book cover design.`;
	}
	if (currTeam === 4) {
		return `We are happy to inform you that the final draft for your ${title} is now ready for publishing and distributing.`;
	}
};
/**
 * Converts a timestamp to object with props time & date
 * @param {Number} timestamp
 * @property {string} time - the time of timestamp to localeString
 * @property {string} date - the time of timestamp to localeString
 * @returns {{time: string, date: string}}
 */
export const timestampToDate = (timestamp) => {
	const event = new Date(timestamp);

	return {
		time: event.toLocaleTimeString('en-UK'),
		date: event.toLocaleDateString('en-UK').replace(/\//g, '.'),
	};
};

/**
 * Shows a success notification to the user.
 * @param {string} message - The message to be shown.
 * @param {string} [title=''] - The title of the message.
 * @returns {void}
 */
export const successNotification = (message, title = '') => {
	showNotification({
		color: 'teal',
		autoClose: 2500,
		title,
		message,
		icon: <Check />,
		radius: 'md',
	});
};

/**
 * Shows a warning notification.
 * @param {string} message - The message to be shown.
 * @param {string} [title=''] - The title of the message.
 * @returns {void}
 */
export const warningNotification = (message, title = '') => {
	showNotification({
		color: 'red',
		autoClose: 2500,
		title,
		message,
		icon: <X />,
		radius: 'md',
	});
};

export const addedToTeamNotification = (message, title = '') => {
	showNotification({
		color: 'orange',
		autoClose: 2500,
		title,
		message,
		radius: 'md',
	});
};
