import {initializeApp} from 'firebase/app';
import {getAuth} from 'firebase/auth';
import {getDatabase} from 'firebase/database';
import {getStorage} from 'firebase/storage';
import {getAnalytics} from 'firebase/analytics';
const firebaseConfig = {
	apiKey: 'AIzaSyCNVHth1tgvxWmiOgPX9aZc7j-mpMLvGbo',
	authDomain: 'publisher-app-5dcb7.firebaseapp.com',
	databaseURL: 'https://publisher-app-5dcb7-default-rtdb.europe-west1.firebasedatabase.app',
	projectId: 'publisher-app-5dcb7',
	storageBucket: 'publisher-app-5dcb7.appspot.com',
	messagingSenderId: '1040642298271',
	appId: '1:1040642298271:web:6c70f4df8b9d092f1dadaa',
};

export const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);

export const db = getDatabase(app);

export const storage = getStorage(app);

export const analytics = getAnalytics()
