# The Publisher

Publisher studio app, which combines all the teams working on the book publishing process and the author together.

The idea behind it comes from our willingness to fasten this process and to facilitate the steps that an author should take and the institutions that should be visited in order to publish a single book.

We hope that this way we could stimulate more authors to submit and publish their books.

## Demo

You can test the app here: https://publisher-app-5dcb7.web.app/

You can use this credentials to login in admin account and to explore all the features:

 author: email - martyozarks@mail.com; pass - asdasd, 
 
 admin: email - dani@dani.com; pass - danikom

Or create new profiles: 

 author: default, when you do not enter code
 employee: 'PubEmp' Enter this code in Code field, while register


## Features
  ![App Screenshot](https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/Screenshot%202022-06-17%20120132.png?alt=media&token=abe60408-95bc-4f0d-8c25-62a5f6ce04e5)
  ![App Screenshot](https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/Our%20team.png?alt=media&token=e17f4f94-c719-4426-bfa1-c5c0bd3d771f)

- Register as an Author or an Employee
Enter the code if you are employee or leave it empty to be an author
  `Employee Register Code: `
- Submit a new Script from Author - Mobile View

  ![App Screenshot](https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/add%20new.png?alt=media&token=b5f509f0-b394-4777-9683-96a249169805)

- Publisher Studio Teams' book approval:

  - Script Review Department - The team will read careful the script and will review if `The Publisher` is the right publisher for the book.
    ![App Screenshot](https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/new%20book.png?alt=media&token=e13ba5cc-e43e-4528-8bf2-ea95f3b661e1)
  - Proposal and Contract Department - The author must send scanned personal documents, the department will send him a contract, which must be signed and returned.
  - Editing and Proof-Reading Department - The team will careful transform the script into book format and will revised all typos.
  - Design & Printing - The team will made the perfect cover for the book.
  - Marketing & Distribution - The team which will ensure that the book will be recognizable and will stand out.
   

- Author's track of the book's state in `The Publisher`'s teams
  ![App Screenshot](https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/author%20.png?alt=media&token=0207b089-ebf5-40ff-8230-8f14a9ffa0d5)
- Team members (Manager's View)
  ![App Screenshot](https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/members.png?alt=media&token=ceb1a3c9-746e-4de3-b448-c33da6acac45)
- Light/dark mode toggle
  ![App Screenshot](https://firebasestorage.googleapis.com/v0/b/publisher-app-5dcb7.appspot.com/o/dark%20mode.png?alt=media&token=4c52495f-d1fa-4924-bdfe-dd1979cb1e96)

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/publisher7/publisher-app.git
```

Go to the project directory

```bash
  cd publisher-app
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run start
```
## Tech Stack

**Client:** React, Mantine, CSS

**Server:** Firebase, EmailJS

## Acknowledgements

- [Telerik Academy ](https://www.telerikacademy.com/)
- [Mantine Library](https://mantine.dev/)

## Authors

- [@stefandimitrov](https://gitlab.com/stefandim91)
- [@danielakomitova](https://gitlab.com/danielankomitova)
